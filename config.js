const theme = require("./themes/main/config.js")
const path = require("path")
const dotenv = require("dotenv")
dotenv.config()
module.exports = {
    theme: theme, // project's theme
    dataDir: path.join(__dirname,"data"), // project's data directory
    assetsDir: path.join(__dirname,"assets"), // project's assets directory
    outDir: path.join(__dirname,"dist"), // project's output directory
    publicUrl: process.env.PUBLIC_URL || "", // project's web url (change it if the project is in sub-folder on your server)
    hljs_langs: [ // highlight.js loaded languages (see: https://github.com/highlightjs/highlight.js/tree/main/src/languages)
        "apache",
        "bash",
        "css",
        "javascript",
        "json",
        "markdown",
        "nginx",
        "php",
        "scss",
        "shell",
        "sql",
        "twig",
        "typescript",
        "xml",
        "yaml",
    ],
    globals: {
        menu: [
            {link: "@/", label: "Home"},
            {link: "@/search", label: "Search"}
        ],
        site_title: "Cours"
    } // global variables for templates
}