const light = getCookie("light-mode")
if(light === null){
    const mq = window.matchMedia('(prefers-color-scheme: dark)')
    if(!mq.matches){
        document.querySelector("body").classList.add("light")
        setCookie("light-mode", true)
    } else {
        setCookie("light-mode", false)

    }
} else {
    if(light === "true") {
        document.querySelector("body").classList.add("light")
    }
}


const lightToggle = document.querySelector(".light-toggle")
lightToggle.addEventListener("click", ()=> {
    setCookie("light-mode", getCookie("light-mode")!=="true")

    document.querySelector("body").classList.toggle("light")
})

function setCookie(cname, cvalue) {
    var d = new Date();
    d.setTime(d.getTime() + (7 * 24 * 60 * 60 * 1000));
    var expires = "expires="+d.toUTCString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}

function getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for(var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return null;
}

const header = document.querySelector("header")
const chapters = Array.from(document.getElementsByTagName("h2"))
let currentActiveChapter = null
let lastPos
window.addEventListener("scroll", evt => {
    if(lastPos < window.scrollY){
        header.classList.add("hidden")
    } else {
        header.classList.remove("hidden")
    }
    let activeChapt = null
    chapters.forEach(chapter => {
        if(chapter.getBoundingClientRect().y <= 150){
            activeChapt = chapter.id
        }
    })
    if(activeChapt !== currentActiveChapter){
        if(currentActiveChapter){
            document.querySelector(`.chapters__link[href="#${currentActiveChapter}"]`).classList.remove("active")
        }
        if(activeChapt){
            document.querySelector(`.chapters__link[href="#${activeChapt}"]`).classList.add("active")
        }
        currentActiveChapter = activeChapt
    }
    lastPos = window.scrollY
})
