const express = require("express")
const path = require("path")
const getPort = require("get-port")
const server = express()
const config = require("../config")
const App = require('../src/App')
const app = new App(config)


server.use(config.publicUrl+'/-/assets', express.static(config.assetsDir));
server.use(config.publicUrl+'/-/theme', express.static(path.join(config.theme.root, "assets")));

server.get(/\s*/, async (req, res) => {
    await app.init()
    const page = await app.renderPage("/"+path.relative(config.publicUrl+"/", req.url))
    // const page = await app.renderPage(req.url)
    res.send(page)

})


Promise.all([getPort({port: getPort.makeRange(8080, 8090)})]).then(([port]) => {
    server.listen(port, () => {
        console.log(`Dev server running at http://localhost:${port}${config.publicUrl}`)
    })
})

