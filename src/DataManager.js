const fs = require('fs/promises')
const path = require('path')

class DataManager {
    pages = {}
    constructor(directory) {
        this.directory = directory
    }
    async load(){
        const files = await this.walk(this.directory)
        const pages = {}
        files.forEach(file => {
            const dir = path.dirname(file)
            const fileName = path.basename(file)
            if(typeof pages[dir] === "undefined"){
                pages[dir] = {}
            }
            switch (fileName){
                case "meta.json":
                case "meta.js":
                    pages[dir]["meta"] = file
                    break
                case "index.html":
                case "index.md":
                case "index.json":
                    pages[dir]["index"] = file
                    break
            }

        })
        const entries =Object.entries(pages).filter(([, page])=>typeof page["index"] !== "undefined")
        this.pages = Object.fromEntries(entries)
        return true
    }
    walk(directoryName) {
        return new Promise(async resolve => {
            const childs = await fs.readdir(directoryName)
            const fileStats = childs.map(async child => {
                    const stat = await fs.stat(path.join(directoryName, child));
                    if (stat.isDirectory()) {
                        return this.walk(path.join(directoryName, child))
                    } else {
                        return "/"+path.relative(this.directory, path.join(directoryName, child))
                    }
                }
            )
            // Promise.all(fileStats).then(resolve)
            Promise.all(fileStats).then(data => resolve(data.flat()))
        })
    }
    getPage(name){
        return this.pages[name]||this.pages["/_404"]||null
    }
}


module.exports = DataManager