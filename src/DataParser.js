const marked = require("marked")
const hljs = require("highlight.js")
const yaml = require("js-yaml")
const fs = require('fs/promises')
const path = require('path')
const {buildChapters} = require("./utils")
const extandedHtmlRenderer = require("./markedRenderers/extandedHtmlRenderer.js")
const plainTextRenderer = require("./markedRenderers/plainTextRenderer.js")

class DataParser {
    extandedHtmlRenderer = extandedHtmlRenderer
    plainTextRenderer = plainTextRenderer

    constructor(publicUrl, dataDir, hljs_langs, globals) {
        hljs_langs.forEach(lang => {
            hljs.registerLanguage(lang, require(`highlight.js/lib/languages/${lang}`));
        })
        this.dataDir = dataDir;
        this.publicUrl = publicUrl;
        this.globals = globals;
    }

    async parsePage(pageDefinition, renderer = extandedHtmlRenderer) {
        const indexContent = await this.parseContent(pageDefinition["index"], renderer)
        const metaContent = pageDefinition["meta"] ? require(path.join(this.dataDir, pageDefinition["meta"])) : {}
        const template = "default"
        const title = path.basename(pageDefinition["index"], path.extname(pageDefinition["index"]))
        return await this.processData({
            template,
            title,
            ...this.globals,
            ...metaContent,
            ...indexContent
        })
    }

    async processData(data) {
        if (data.tokens) {
            data.chapters = buildChapters(data.tokens)
        }
        return data
    }

    async parseContent(src, renderer) {

        marked.use({renderer})

        src = path.join(this.dataDir, src)
        const ext = path.extname(src)
        if (ext === ".json") {
            return require(src)
        }
        const data = await fs.readFile(src, "utf8")
        let {content, meta} = this.parseContentMeta(data);
        let tokens = null
        if (ext === ".md") {
            tokens = marked.lexer(content)
            content = marked(content)
            // content = JSON.stringify(tokens)
        }

        return {
            ...meta, content, tokens
        }
    }

    parseContentMeta(data) {
        const findRegex = new RegExp("^(?<=---\\n)[\\s\\S\\n]*?(?=\\n---\\n)", "m");
        const replaceRegex = new RegExp("^(---\\n)[\\s\\S\\n]*?(\\n---\\n)", "m");
        const match = data.match(findRegex)
        if (match) {
            const content = data.replace(replaceRegex, "")
            const meta = yaml.load(match[0])
            return {
                content, meta
            }
        }
        return {
            content: data,
            meta: {}
        }
    }
}

module.exports = DataParser