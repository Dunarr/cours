module.exports = {
    code(txt) {
        return ` ${txt} `
    },
    blockquote(txt) {
        return ` ${txt} `
    },
    html(txt) {
        return ` ${txt.replace(/(<([^>]+)>)/gi, "")} `
    },
    heading(txt) {
        return ` ${txt} `
    },
    hr() {
        return ""
    },
    list(body) {
        return ` ${body} `
    },
    listitem(txt) {
        return ` ${txt} `
    },
    checkbox() {
        return ""
    },
    paragraph(txt) {
        return ` ${txt} `
    },
    table(header, body) {
        return ` ${header} ${body} `
    },
    tablerow(body) {
        return ` ${body} `
    },
    tablecell(txt) {
        return txt
    },
    strong(txt) {
        return txt
    },
    em(txt) {
        return txt
    },
    codespan(txt) {
        return txt
    },
    br() {
        return ""
    },
    del(txt){
        return txt
    },
    link(_1,_2,txt){
        return txt
    },
    image(_1,_2,txt){
        return txt
    },
    text(txt){
        return txt
    },
}