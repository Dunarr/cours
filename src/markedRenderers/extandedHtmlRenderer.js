const hljs = require("highlight.js")
const {slugify, stripHtml} = require("../utils")
const Renderer = require("marked/src/Renderer");

const footnoteMatch = /^\[\^([^\]]+)\]:([\s\S]*)$/;
const referenceMatch = /\[\^([^\]]+)\](?!\()/g;
const referencePrefix = "fn-ref";
const footnotePrefix = "fn";
const footnoteTemplate = (ref, text) => {
    return `<sup id="${footnotePrefix}:${ref}">${ref}</sup>${text}`;
};
const footnoteContainerTemplate = (text) => {
    return `<div class="marked-footnotes">${text}</div>`
}
const referenceTemplate = ref => {
    return `<sup id="${referencePrefix}:${ref}"><a href="#${footnotePrefix}:${ref}">${ref}</a></sup>`;
};
const interpolateReferences = (text) => {
    return text.replace(referenceMatch, (_, ref) => {
        return referenceTemplate(ref);
    });
}
const interpolateFootnotes = (text) => {
    const found = text.match(footnoteMatch)
    if (found) {
        const replacedText = text.replace(footnoteMatch, (_, value, text) => {
            return footnoteTemplate(value, text);
        });
        return footnoteContainerTemplate(replacedText)
    }
    return text
}

const defaultRenderer = new Renderer()
const renderer = {
    ...defaultRenderer,
    paragraph(text) {
        return defaultRenderer.paragraph.apply(null, [
            interpolateReferences(interpolateFootnotes(text))
        ]);
    },
    text(text) {
        return defaultRenderer.text.apply(null, [
            interpolateReferences(interpolateFootnotes(text))
        ]);
    },
    code(code, lang, escaped) {


        if (!lang || !hljs.autoDetection(lang)) {
            return '<div class="hljs"><pre><code>'
                + code
                + '</code></pre></div>\n';
        }

        let langName = null
        if (hljs.autoDetection(lang)) {
            langName = hljs.getLanguage(lang).name;
            code = hljs.highlight(code, {language: lang}).value
        }


        return `<div class="hljs">
            <code class="langname">${langName}</code>
            <pre><code class="${this.options.langPrefix} ${escape(lang)}">${code}</code></pre>
        </div>\n`;
    },
    heading(text, level) {
        if (this.options.headerIds) {
            return '<h'
                + level
                + ' id="'
                + slugify(stripHtml(text))
                + '">'
                + text
                + '</h'
                + level
                + '>\n';
        }
        // ignore IDs
        return '<h' + level + '>' + text + '</h' + level + '>\n';
    },
    link(href, title, text) {
        if (href === null) {
            return text;
        }
        let out = '<a href="' + href + '"';
        if (title) {
            out += ' title="' + title + '"';
        }
        if (href.match(/^https?:\/\//)) {
            out += 'target="_blank"'
        }
        out += '>' + text + '</a>';
        return out;
    }
};
module.exports = renderer
