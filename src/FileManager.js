const fs = require("fs");
const path = require("path");
const fse = require("fs-extra");

class FileManager {

    constructor(outdir) {
        this.outDir = outdir
    }

    copyDir(dir, out) {
        return new Promise((resolve, reject) => {
            fse.copy(dir, path.join(this.outDir, out), function (err) {
                if (err) {
                    return reject(err)
                }
                resolve(true)
            });

        })

    }

    mkdir(dirPath) {
        return new Promise((resolve, reject) => {
            fs.access(dirPath, fs.constants.F_OK, (err) => {
                if (err) {
                    fs.mkdir(dirPath,
                        {recursive: true}
                        , err => {

                            if (err) {
                                return reject(err)
                            }
                            resolve(true)
                        })
                } else {
                    resolve(true)
                }
            })
        })
    }

}
module.exports = FileManager