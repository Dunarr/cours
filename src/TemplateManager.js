const fs = require("fs/promises")
const path = require("path")
const Mustache = require("mustache")

Mustache.escape = t=>t
class TemplateManager {
    templates = {}

    constructor(themeConfig, publicUrl) {
        this.theme = themeConfig;
        this.publicUrl = publicUrl;
    }

    async load() {
        const templates = {}
        const partials = {}
        const partialsDir = path.join(this.theme.root, "partials")
        const templatesDir = path.join(this.theme.root, "layouts")
        const partialsNames = await fs.readdir(partialsDir)
        const fileNames = await fs.readdir(templatesDir)

        await Promise.all(partialsNames.map(async file => {
            partials[path.basename(file, path.extname(file))] = this.preprocessTemplate(await fs.readFile(path.join(partialsDir, file), "utf8"))
            })
        )

        await Promise.all(fileNames.map(async file => {
                templates[path.basename(file, path.extname(file))] = this.preprocessTemplate(await fs.readFile(path.join(templatesDir, file), "utf8"))
            })
        )
        this.templates = templates
        this.partials = partials
        return true;
    }

    async render(data) {
        if (typeof this.theme.preprocess === "function") {
            data = this.theme.preprocess(data)
        }
        const template = this.templates[data.template]
        const result = Mustache.render(template, data, this.partials)
        return this.postProcessPage(result)
    }

    preprocessTemplate(content) {
        // return content.replace(/@@/g, publicUri)
        return content
            .replace(/(?<=[^\\])@@\//g, this.publicUrl + "/-/theme/")
    }

    postProcessPage(content) {
        // return content.replace(/@@/g, publicUri)

        return content
            .replace(/(?<=[^\\])@@\//g, this.publicUrl + "/-/assets/")
            .replace(/\\@@\//g, "@@/")
            .replace(/(?<=[^\\@])@\//g, this.publicUrl+"/")
            .replace(/\\@\//g, "@/")
    }
}

module.exports = TemplateManager