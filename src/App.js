const path = require("path");
const fs = require("fs/promises");
const FileManager = require("./FileManager")
const DataManager = require("./DataManager")
const DataParser = require("./DataParser")
const TemplateManager = require("./TemplateManager")
const lunr = require("lunr")

class App {
    static EVENTS = {
        LOAD_CONFIG: "LOAD_CONFIG",
        INIT_MANAGERS: "INIT_MANAGERS",
        POST_INIT_MANAGERS: "POST_INIT_MANAGERS",
        INIT_APP: "INIT_APP",
        POST_INIT_APP: "POST_INIT_APP",
        CREATE_OUTDIR: "CREATE_OUTDIR",
        LOAD_PAGE: "LOAD_PAGE",
        LIST_PAGES: "LIST_PAGES",
        PARSE_PAGE: "PARSE_PAGE",
        RENDER_PAGE: "RENDER_PAGE",
        POST_RENDER_PAGE: "POST_RENDER_PAGE",
        SAVE_PAGE: "SAVE_PAGE",
        APP_BUILT: "APP_BUILT",
    }
    eventListeners = {}

    constructor(config) {
        config = {
            ...{
                theme: require("../themes/main/config"),
                plugins: [],
                dataDir: path.join(process.cwd(), "data"),
                outDir: path.join(process.cwd(), "dist"),
                publicUrl: process.env.OUT_DIR || "",
                hljs_langs: [],
                globals: {}
            },
            ...config
        }
        if (typeof config.theme.init === "function") {
            config.theme.init(this, App.EVENTS)
        }
        config.plugins.forEach(plugin => {
            if (typeof plugin.init === "function") {
                plugin.init(this)
            }
        })
        this.triggerEvent(App.EVENTS.LOAD_CONFIG)
        this.config = config
        this.triggerEvent(App.EVENTS.INIT_MANAGERS)
        this.dataParser = new DataParser(config.publicUrl, config.dataDir, config.hljs_langs, config.globals)
        this.fileManager = new FileManager(config.outDir)
        this.dataManager = new DataManager(config.dataDir)
        this.templateManager = new TemplateManager(config.theme, config.publicUrl)
        this.triggerEvent(App.EVENTS.POST_INIT_MANAGERS)
    }

    async init() {
        this.triggerEvent(App.EVENTS.INIT_APP)
        await Promise.all([
            this.dataManager.load(),
            this.templateManager.load()
        ])
        this.triggerEvent(App.EVENTS.POST_INIT_APP)
        return true;
    }

    triggerEvent(eventName, data) {
        if (!Array.isArray(this.eventListeners[eventName])) {
            return Promise.resolve(data)
        }
        return this.eventListeners[eventName].sort((l1, l2) => l1.priority - l2.priority).map(l => l.callback).reduce((stack, promise) => stack.then(promise), Promise.resolve(data))
    }

    addEventListener(eventName, callback, priority = 1) {
        if (!Array.isArray(this.eventListeners[eventName])) {
            this.eventListeners[eventName] = []
        }
        this.eventListeners[eventName].push({
            priority,
            callback
        })
    }

    async renderPage(url) {
        this.triggerEvent(App.EVENTS.LOAD_PAGE)
        const page = this.dataManager.getPage(url)
        this.triggerEvent(App.EVENTS.PARSE_PAGE)
        const data = await this.dataParser.parsePage(page)
        this.triggerEvent(App.EVENTS.RENDER_PAGE)
        const rendered = await this.templateManager.render(data)
        this.triggerEvent(App.EVENTS.POST_RENDER_PAGE)
        return rendered

    }

    async build() {
        this.triggerEvent(App.EVENTS.CREATE_OUTDIR)
        await this.fileManager.mkdir(this.config.outDir)
        this.triggerEvent(App.EVENTS.LIST_PAGES)
        const pagesPromises = Object.entries(this.dataManager.pages).map(async ([name, page]) => {
            name = path.join(this.config.outDir, name)
            this.triggerEvent(App.EVENTS.PARSE_PAGE)
            const data = await this.dataParser.parsePage(page)
            this.triggerEvent(App.EVENTS.RENDER_PAGE)
            const rendered = await this.templateManager.render(data);
            this.triggerEvent(App.EVENTS.POST_RENDER_PAGE)
            this.triggerEvent(App.EVENTS.SAVE_PAGE)
            await this.fileManager.mkdir(name)
            await fs.writeFile(path.join(name, "index.html"), rendered)
            return Promise.resolve(true)
        })
        pagesPromises.push(this.fileManager.copyDir(this.config.assetsDir, path.join("-", "assets")))
        pagesPromises.push(this.fileManager.copyDir(path.join(this.config.theme.root, "assets"), path.join("-", "theme")))

        const result = await Promise.all(pagesPromises)
        this.triggerEvent(App.EVENTS.APP_BUILT)

        const indexePromises = Object.entries(this.dataManager.pages).map(async ([name, page]) => {
            const data = await this.dataParser.parsePage(page, this.dataParser.plainTextRenderer)
            return Promise.resolve({content: data.content?.toLowerCase(), path: this.config.publicUrl + name, title: data.title})
        })

        const pages = await Promise.all(indexePromises)

        const pageContents = {}

        const idx = lunr(function () {
            this.ref('path')
            this.field('content')
            this.field('title',{boost: 1.5})
            this.metadataWhitelist = ["position"]
            pages.forEach(function (page) {
                pageContents[page.path] = page
                this.add(page)
            }, this)
        })
        await fs.writeFile(path.join(this.config.outDir, "_index.json"), JSON.stringify({index: idx, pages: pageContents}))
        const searchpage = await this.templateManager.render({...this.config.globals ,template: "_search", title: "Search"})
        await this.fileManager.mkdir(path.join(this.config.outDir, "/search"));
        await fs.writeFile(path.join(this.config.outDir, "/search", "index.html"), searchpage)


        return result;
    }
}

module.exports = App