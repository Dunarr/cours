function slugify(str)
{
    str = str.replace(/^\s+|\s+$/g, '');

    // Make the string lowercase
    str = str.toLowerCase();

    // Remove accents, swap ñ for n, etc
    var from = "ÁÄÂÀÃÅČÇĆĎÉĚËÈÊẼĔȆÍÌÎÏŇÑÓÖÒÔÕØŘŔŠŤÚŮÜÙÛÝŸŽáäâàãåčçćďéěëèêẽĕȇíìîïňñóöòôõøðřŕšťúůüùûýÿžþÞĐđßÆa·/_,:;";
    var to   = "AAAAAACCCDEEEEEEEEIIIINNOOOOOORRSTUUUUUYYZaaaaaacccdeeeeeeeeiiiinnooooooorrstuuuuuyyzbBDdBAa------";
    for (var i=0, l=from.length ; i<l ; i++) {
        str = str.replace(new RegExp(from.charAt(i), 'g'), to.charAt(i));
    }

    // Remove invalid chars
    str = str.replace(/[^a-z0-9 -]/g, '')
        // Collapse whitespace and replace by -
        .replace(/\s+/g, '-')
        // Collapse dashes
        .replace(/-+/g, '-');

    return str;
}

function buildChapters(tokens) {

    const chapters = []
    for (let i = 0; i < tokens.length; i++) {
        const token = tokens[i]
        if (token.type === "heading" && token.depth === 2) {
            chapters.push({
                id: i,
                slug: slugify(stripHtml(getText(token))),
                label: getText(token),
            })
        }
    }
    return chapters
}
function getText(token){
    let texts = ""
    if(typeof token.tokens != "undefined"){
        token.tokens.forEach(item =>{
            texts+=getText(item)
        })
    } else {
        texts+=token.text
    }
    return texts
}
function stripHtml(html)
{
    return html.replace(/((?:<)([^>]+)(?:>))/ig,"").replace(/(?:&lt;)|(?:&gt;)/ig,"");
}

module.exports = {
    slugify,
    buildChapters,
    stripHtml
}