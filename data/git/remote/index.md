---
title: Dépots de code en ligne
---
On peut partager notre code sur internet via des "dépôts de code en ligne".
Les principaux sont:
 - github
 - gitlab
 - bitbucket

Ces outils me permettent de travailler à plusieur 
ou de publier mon code pour que chacun puisse y accéder


## Envoie de mon code en ligne.
Si on a un dépot de code existant sur notre ordinateur,
on peut l'envoyer sur un dépot en ligne pour celà, il faut créer le projet en ligne.   
Ensuite, dans le dossier de mon projet, il faut tapper les commandes suivantes:
```bash
git remote add origin URL_DU_DEPOT
git push -u origin --all
```

l'**URL_DU_DEPOT** est une information qui est donnée par le site (sur gitlab, il s'ageit du boutton bleu "clone")

## Envoie des mises à jour du code
Avec git, on peut envoyer les nouveaux **commits** en ligne avec la commande:
```bash
git push
```

## Récupération du code en ligne
Pour télécharger le code qui est hébergé sur un dépot en ligne, on peut taper la commande suivante

```bash
git clone URL_DU_DEPOT NOMDOSSIER
```
Cette commande créra un dossier en fonction du nom donné.

Si on a un dossier (vide) existant, on peut aussi mettre le code dedans, 
pour ça, il faut se placer dans le dossier en question et taper
```bash
git clone URL_DU_DEPOT .
```

Enfin, si on a déjà "cloné" le projet, on peut télécharger 
les mises à jour envoyées par les autres développeurs avec la commande:
```bash
git pull
```

## Travail à plusieurs avec des branches
 - se mettre sur la branche master et faire un pull
 - créer une branche pour nous et se mettre dessus
 - faire des commits sur notre branche
 - envoyer nos commits avec push[^1]
 - fusionner la branche avec la branche principale 
   en faisant une "merge request"/"pull request"
   une fois que ma fonctionnalité est finie

[^1]: si la branche n'existait pas avant sur le dépôt, il faut taper `git push -u origin NOM_BRANCHE`