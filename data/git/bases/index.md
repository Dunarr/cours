---
title: Gestion de version
---

## Git
Git est un logiciel de gestion de version
Il permet de conserver et sauvegarder un historique
des modifications effectuées dans un projet

Il nous permet aussi de partager son code
en ligne de manière publique ou non 

Les principales utilités de git sont :
 - de garder un historique de ce qui c'est passé dans mon projet
 - de sauvegarder mon projet et pouvoir revenir en arrière
 - sauvegarder mon travail en ligne pour le conserver,
 le partager ou travailler à plusieurs


## Les commits

Un commit est la sauvegarde d'une 
ou plusieurs modification dans des fichiers 
parmis lesquels:
- la création d'un fichier
- la suppression d'un fichier
- la création d'une ligne de code
- la suppression d'une ligne de code
- la modification d'une ligne de code

Git ne prend pas en compte les actions faites sur les dossiers
(si un fichier est dans un dossier,
il conservera cependant cette information)

## création de projet
pour initialiser un **dépôt git (ou repository)**,
il faut taper la commande suivante dans le dossier du projet
```bash
git init
```

## Status du projet

Pour savoir ou j'en suis dans mon projet
(quels fichiers sont modifiés, quels commits on étés faits),
on peut utiliser la commande:
```bash
git status
```

## Sauvegarde des modifications

Après avoir modifié des fichiers,
pour sauvegarder les modifications, il y a plusieures étapes à réaliser:

#### Indiquer à git quelles modifications je veux sauvegarder (staging)
```bash
git add NOM_DU_FICHIER
```
Avec cette commande, on va ajouter d'un seul coup
toutes les modifications  apporté au fichier indiqué

#### Crééer le commit avec les modifications indiquées précédemment
 ```bash
git commit -m "description de ce que j'ai fait"
```
<asciinema-player src="@@/git.cast" rows="30"></asciinema-player>



## Lire l'historique de mes modifications

Pour Voir la liste des commits (donc des modification) dans un dépôt
on peut utiliser la commande :
```bash
git log
```

Elle nous donne pour chaque commit réalisé
 - l'identifiant du commit 
 - L'auteur
 - la date de création
 - le message du commit


Pour Voir le contenu d'un commit 
(toutes les modifications qui ont été faites dedans)
on peut utiliser la commande :

```bash
git show IDENTIFIANT_DU_COMMIT
```


## Gestion des branche

Git me permet de gérer plusieures version alternatives d'un même projet.
On appel ça des **branches**. on va donc créer des branches. 
À ce moment, on créé une version alternative du code qui (pour l'instant)
contient la même chose que la version de base

pour créer une branche, on utilise la commande:
```bash
git branch NOM_DE_LA_BRANCHE
```
/!\ les noms de branche ne doivent contenir
que des lettres/chiffres/underscores/slash


On pourra ensuite passer d'une version à l'autre de mon programme 
grace à la commande: 
```bash
git checkout NOM_DE_LA_BRANCHE
```

Un commit est réalisé sur une branche et par defaut 
ne sera pas disponnible sur les autres.

Une fois que tout est fini sur ma branche, je vais pouvoir la fusionner avec
une autre (souvent la branche principalle)   
pour ça, je doit me placer dans la branche qui **reçoit** les modifications.
ensuite taper:
```bash
git merge NOM_DE_LA_BRANCHE_A_IMPORTER
```

## Gestion des conflits
Lorsque sur deux branches on fait des modifications sur les mêmes lignes d'un fichiers,
on peut avoir un conflit au moment de les fusionner.
Lorsqu'on a des conflits, git nous les indique de cette manière:
```html
<<<<<<< HEAD 
    <div>du contenu -version 1-</div> (code de la version actuelle)
=======
    <div>du contenu - contenu num 2 -</div> (code que je veux fusionner)
>>>>>>> version_2
```

il faudra indiquer quoi garder dans le code ou comment fusonner le tout en modifiant le fichier.   
Une fois le conflit résolu, on ajoute le fichier avec un `git add NOM_FICHIER`   
Une fois tout les conflit résolu, 
on valide le merge avec un `git commit`

Enfin git nous ouvre un editeur pour valider le merge, il suffit en général de valider et quitter

si on a nano, il faut faire:
 - ctrl + o
 - Entrée
 - ctrl + x

si on a vim, il faut faire:
- :wq


## Résumé git
![example image](@@/git-summary.png "Résumé git")
_Résumé git par [openclassroom](https://openclassrooms.com/fr/courses/7162856-gerez-du-code-avec-git-et-github/7165643-recapitulez-ce-que-vous-avez-appris) [cc](https://creativecommons.org/licenses/by-sa/4.0/)_



## gitignore

o peut indiquer à git d'ignorer (ne pas versionner) certains fichiers,
pour ça, il suffit de créer un fichier `.gitignore` dans le projet 
et d'indiquer le nom des fichiers à ignorer, 
on peut ignorer un dossier entier en ajoutant `/` à la fin du nom.
cela permet d'ignore les fichier contenant des données sensibles ou trop lourdes pour un dépôt git

## Commandes supplémentaires

-`git branch` affiche la liste des branches, et indique sur laquelle on est actuellement
-`git reflog` donne la liste de toutes les actions réalisées dans le dépôt
- `git revert ID_COMMIT` permet d'annuler le contenu d'un commit
- `git reset --hard` permet de supprimer toutes les modifications depuis le dernier commit
- `git add .` permet d'ajouter tout les fichier, pour préparer un commit

## Annexes
 - [git cheatsheet (duckduckgo)](https://duckduckgo.com/?t=ffab&q=git+cheat+sheet&ia=cheatsheet&iax=cheatsheet)
 - [documentation git](https://git-scm.com/doc)