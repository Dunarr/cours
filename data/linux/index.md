---
title: Intro Linux
---

# Distributions de linux

Étant donné que linux est **libre**, la communauté a créé plusieures versions de l'os (**distributions**) avec chacune ses spécificitées

Examples de distributions:
- Ubuntu
- Debiam
- Arch
- Red Hat
- Suze

# Structure de dossiers linux

Sur linux, l'intégralité des fichiers se trouvent dans le dossier "/" (racine) de l'ordinateur (peut importe sur quel disque / support ils se trouvent)
dans le dossier racine, on retrouve différents dossiers principaux, utilisés par le système d'exploitation:

- `bin` => programmes (principalement en ligne de commandes)

- `boot` => fichiers pour le lancement

- `dev` => fichiers spéciaux utilisés par l'os (générateur de nombres aléatoires, statut du cpu / de la mémoire)

- `etc` => configuration de l'ordinateur

- `home` => dossier personnel

- `media` => peripheries externes

- `root` => dossier personnel de l'administrateur

- `snap` => programmes installés avec snap

- `tmp` => fichiers temporaires

- `usr` => programmes spécifiques aux utilisateurs

- `var` => fichiers divers (dont les logs system)



## Example de structure de fichier sur linux
```
   / -- home/ -- moi/ -- Documents/
     |                |
     |                -- Downloads/
     |                |
     |                -- Desktop/ -- mon-dossier/
     |                             |
     |                             -- mon-fichier.txt
     |                             |
     |                             -- mon-image.jpg
     |
     -- var/
     |
     -- root/
```

Le nom complet d'un fichier ou dossier est son **chemin absolut** (chemin à partir du dossier racine, il commence par /)
on peut retrouver un fichier ou dossier par son **chemin relatif** (chemin depuis le dossier actuel)
ou par son chemin absolut






# Ligne de commande

## Chemins
Commande `pwd` affiche le dossier actuel

### chemin relatif (depuis le dossier /home/moi):
- `Desktop/` => `/home/moi/Desktop/`
- `Desktop` => `/home/moi/Desktop/`
- `Desktop/mon-image.jpg` => `/home/moi/Desktop/mon-image.jpg`
- `..` => `/home/` (dossier parent)
- `../..` => `/`
- `.` => `/home/moi/` (dossier actuel)
- `.`/Desktop => `/home/moi/Desktop/`
- `../../var` => `/var/`

### chemin absolut (le même peut import où on se trouve):
- `/`
- `/var`
- `/home/moi/Desktop/mon-fichier.txt`
- `~` => `/home/moi/`
- `~/Desktop/mon-dossier` => `/home/moi/Desktop/mon-dossier/`

## Commandes
- `cd` => se déplacer
- `touch` => créer un fichier
- `mkdir` => créer un dossier
- `ls` => lister les fichier / dossiers,
    - `-l` => voir les details
    - `-a` => voir les fichiers cachés
    - `-d` => voir le dossier dans lequel on a fait le ls
    - `-h` => voir les tailles en Go/Mo/Ko
- `cp` => copier un fichier
    - `-r` => copier un dossier
- `mv` => renome/déplacer un dossier/fichier
- `rm` => supprimer un fichier
    - `-r` => supprimer un dossier
- `nano` => éditer un fichier
- `cat` => lire un fichier
- `tail` => lire la fin d'un fichier
- `top` => gestionnaire de tache
- `ping` => interoger un serveur par son nom de domaine
- `lsb_release -a` => indique quelle version de linux est installée
- `chmod` => modifie les permissions d'un fichier / dossier
    - `-R` => pour appliquer de manière récursive (à l'interieur d'un dossier)
- `chown` => modifie les l'utilisateur / groupe qui possède un fichier
    - `-R` => pour appliquer de manière récursive (à l'interieur d'un dossier)
- `wget` => télécharger un fichier
- `tar` => gérer des fichier tar (archives)
    - `-xf ` => extraire une archive (tar -xf MON_ARCHIVE.tar.gz)
    - `-czf` => créer une archive (tar -czf MON_ARCHIVE.tar.gz FICHIER1 FICHIER2 ...)
- `echo` => afficher du texte


- `sudo apt install` => installer un programme
- `sudo apt remove` => supprimer un proggramme
- `sudo apt update` => chercher les MAJs
- `sudo apt upgrade` => installer les MAJs (après un update)
- `sudo apt autoremove` => supprimer les dépendences non utilisées

- `sudo snap install` => installer un programme
- `sudo snap remove` => supprimer un proggramme

- image magick : https://imagemagick.org/script/command-line-tools.php

### Aguments
- `*` => wildcard, permet de séléctionner plusieurs éléments

- `mon\ dossier` = permet de mettre un espace
- `"mon dossier"` = permet de mettre un espace


exemples:
 - `dossier/*` => tout les éléments d'un dossier
 - `*.jpeg` => tout ce qui fini par .jpeg
 - `fichier*` => tout ce qui commence par fichier

### Autre
 - `COMMANDE --help` => aide sur le programme (si le programme le propose)
 - `man COMMANDE` => aide sur le programme

## Droits
Linux permet de gérer les droits de tout les fichier / dossier du système.

Pour cela, chaque fichier possède un ensemble de permissions et un propriétaire (owner)

Un utilisateur peut avoir differents niveau de droit sur un fichier
 - `owner`: l'utilisateur possède le fichier (un fichier ne peut appartenir qu'à un seul utilisateur)
 - `group`: l'utilisateur appartien au groupe qui possède le fichier (un fichier ne peut appartenir qu'à un seul groupe, mais un groupe peut contenir plusieurs utilisateurs)
 - `autre`: l'utilisateur n'a aucun lien avec le fichier

pour chaque niveau, on peut définire ce que l'utilisateur peut faire
 - `r`: `read` => l'utilisateur peut lire le contenu du fichier/dossier
 - `w`: `write` => l'utilisateur peut lire le contenu du fichier / ajouter un fichier ou dossier dans le dossier
 - `x`: `execute` => l'utilisateur peut executer le fichier (lancer un programme) / rentrer et voir ce qui est dans le dossier


### chown
Gestion du propriétaire / groupe

`chown USER:GROUP NOM_FICHIER`

### chmod
Gestion des permissions


`sudo chmod (DROIT) NOM_FICHIER`

```
système de droits en binaire
u   g   o   (a)
rwx r-x r-- => rwx r.x r..
421 421 421 => 421 4.1 4.. => 754
```

examples:
```shell
sudo chmod 754 fichier.txt
sudo chmod u+x fichier.txt
sudo chmod a-r fichier.txt
```


## Gestion des paquets

[Liste des paquets apt](https://packages.ubuntu.com/)


 - `sudo apt install NOM_DU_PAQUET` => installation d'un paquet
 - `sudo apt remove NOM_DU_PAQUET` => suppression d'un paquet
 - `sudo apt autoremove` => suppression des dependences non utilisées
 - `sudo apt update` => récuperation des MAJs
 - `sudo apt upgrade` => installation des MAJs (utiliser après un sudo apt update)


[Liste des paquets snap](https://snapcraft.io/)


 - `sudo snap install NOM_DU_PAQUET (--classic)` => installation d'un paquet snap