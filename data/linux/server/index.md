---
title Mise en production & gestion de serveur
---
## Intro
Lorsque l'on développe un site web, si l'on sohaite le rendre disponnibe en ligne, il me faut principallement 2 éléments:
- **Un serveur** (un pc accssible publiquement, qui tourne 24/24 et avec les bons logiciels installé)
- **Un nom de domaine**, que l'on pourra taper pour accéder au serveur (pas obligatoir, mais plus simple à utiliser qu'une ip)

## Nom de domaine
## Serveur
## Configuration de serveur

Un serveur est un ordinateur qui (en général) est installé sous linux avec rien dessus.
Il va donc faloire installer un logiciel qui me servira des pages web quand je l'appel (apache ou nginx par example). Il faut aussi installer le programme permettant  de lire mes scripts (PHP)

> On peut utiliser d'autres langages que PHP pour créer un site web   
> js (avec Node / express)   
> c# (avec ASP .net)   
> Java (avec JEE)   
> Python (avec Django)

### Installation apache + PHP
Sur ubuntu, pour installer php + apache, il suffit de faire:
```shell
sudo apt install php
```
Vous pouvez installer la version que vous voulez de hp pen utilisatn un dépot custom: [tuto](https://maximepinot.com/en/blog/how-to-install-the-latest-version-of-php-under-ubuntu-on-windows-wsl.html)

### Installation de la base de données
En général, on aurra besoin d'une base de données sur nos sites, pour celà, on utilisera une Base de données (abrége en BDD ou DB).
Si on on fait un site en PHP, on utilisera principallement **Mysql** ou **MariaDB**.
Pour installer mariaDb, il faut taper (en général, on préfère installer mariaDB à mysql, mais les deux font la même chose)
```shell
sudo apt install mariadb-server
```

L'installation des trois logiciels (Apache, Mariabd, Php) est en général appelé "**stack AMP**".
Une "stack LEMP" est l'équivalent mais avec Nginx à la placce de Apache.

### Configuration de virtualhosts
Sur un serveur, on peut faire fonctionner plusieurs sites webs différents en même temps.
Par example, si je veux lancer mon site perso (dunarr.com) et mon site pro (super-dev.com) sur le même serveur.
Pour celà, il faut configurer de **vhosts** sur apache, il s'agit de configuration permetant d'indique à apache ou aller chercher les ficher du site en fonction du nom de domaine demandé.

Pour créer un vhost, il faut créer un fichier `.conf` dans le dossier `/etc/apache2/sites-available`.
Dans ce fichier, il faut mettre les instructions suivantes:
```apacheconf
<VirtualHost *:80>
    ServerName dunarr.com # Nom de domaine attendu pour cette config
    DocumentRoot /var/www/site-perso # Où aller cherche les fichiers du site
</VirtualHost>
```

Un fois que la config est créée, il faut l'activer:
```shell
sudo a2ensite monsite.conf # <== Nom du fichier de config créé
```

Lorsque l'on créée un nouvelle config ou qu'on en modifie une, il est recommandé de la verifier pour éfiter que le serveur plante,
avec la commande:
```shell
sudo apachectl configtest
```

Enfin, il faut redémarrer apache
```shell
sudo systemctl reload apache2
```


### Ajouter l'https

Pour activer l'https sur un site il nous faut un certificat ssl.
Un certificat ssl est un "document numérique" qui sert de certificat d'authenticité pour un nom de domaine,
il est relié à un et un seul nom de domaine. Il est délivré par une autorité externe.
On peut en acheter ou en obtenir gratuitement auprès de certains fournisseurs.

Pour avoir un certificat gratuit pour notre site, on utilise `certbot`

Pour installer certbot (avec l'extension pour apache) :
```shell
sudo apt install certbot
sudo apt install python3-certbot-apache
```

Il faut aussi activer l'extension rewrite pour rediriger l'utilisateur sur la version https
pour ça, taper:
```shell
sudo a2enmod rewrite
```

Pour générer un certificat:
```shell
sudo certbot
```


## Gestion des erreurs en production
Sur un serveur en prod, on ne souhaite pas afficher les erreurs dans la page (l'utilisateur n'a pas à avoir des infos sur mon code).
Cependant, il arrive que le code contienne des erreurs, pour les voir, on utilisera un fichier de logs sur le serveur.
Il s'agit d'un fichier dans lequel serront écrites les erreurs.

pour activer les logs d'erreur sur un site, il suffit de modifier une ligne dans le fichier `php.ini`:
```ini
display_errors = On
```