---
title: Plans de tests
---

## Table des matières
<!-- TOC -->
  * [Table des matières](#table-des-matières)
  * [Introduction aux tests logiciels](#introduction-aux-tests-logiciels)
    * [Définition et objectifs](#définition-et-objectifs)
    * [Importance des tests](#importance-des-tests)
    * [Niveaux de tests](#niveaux-de-tests)
    * [Types de tests](#types-de-tests)
      * [Tests fonctionnels](#tests-fonctionnels)
        * [Tests unitaires](#tests-unitaires)
        * [Tests d'intégration](#tests-dintégration)
        * [Tests système ou tests e2e](#tests-système-ou-tests-e2e)
        * [Tests d'acceptation](#tests-dacceptation)
      * [Tests non-fonctionnels](#tests-non-fonctionnels)
  * [Le cycle de vie des tests](#le-cycle-de-vie-des-tests)
    * [Planification](#planification)
    * [Conception](#conception)
    * [Implémentation et exécution](#implémentation-et-exécution)
    * [Évaluation et reporting](#évaluation-et-reporting)
    * [Clôture](#clôture)
  * [Élaboration d'un plan de tests](#élaboration-dun-plan-de-tests)
    * [Structure et composants](#structure-et-composants)
    * [Critères d'entrée et de sortie](#critères-dentrée-et-de-sortie)
    * [Environnement de test](#environnement-de-test)
  * [Techniques de conception des cas de tests](#techniques-de-conception-des-cas-de-tests)
    * [Tests boîte noire vs boîte blanche](#tests-boîte-noire-vs-boîte-blanche)
    * [Partitionnement en classes d'équivalence](#partitionnement-en-classes-déquivalence)
    * [Analyse des valeurs limites](#analyse-des-valeurs-limites)
    * [Table de décision](#table-de-décision)
    * [Tests basés sur les scénarios utilisateurs](#tests-basés-sur-les-scénarios-utilisateurs)
  * [Tests unitaires](#tests-unitaires-1)
    * [Principes et bonnes pratiques](#principes-et-bonnes-pratiques)
    * [Framework de test](#framework-de-test)
    * [TDD (Test-Driven Development)](#tdd-test-driven-development)
  * [Tests d'intégration](#tests-dintégration-1)
    * [Intégration des composants](#intégration-des-composants)
    * [Tests d'API](#tests-dapi)
    * [Mocking](#mocking-)
  * [Tests de bout en bout (E2E)](#tests-de-bout-en-bout-e2e)
    * [Principes et challenges](#principes-et-challenges)
    * [Outils populaires](#outils-populaires)
  * [Tests non-fonctionnels](#tests-non-fonctionnels-1)
    * [Tests de performance](#tests-de-performance)
  * [Bibliographie](#bibliographie)
<!-- TOC -->

---

## Introduction aux tests logiciels

### Définition et objectifs

Les tests logiciels constituent un processus systématique visant à évaluer la qualité d'une application et à réduire les risques de défaillance. Ils permettent de vérifier que le logiciel répond aux exigences spécifiées et de détecter les anomalies avant la mise en production.

**Objectifs principaux des tests :**

- Valider que l'application répond aux exigences fonctionnelles et non-fonctionnelles
- Identifier les défauts et les bugs
- Prévenir les régressions lors des évolutions
- Fournir une confiance dans la qualité du produit
- Réduire les coûts de maintenance

### Importance des tests

Un testing rigoureux est indispensable pour plusieurs raisons :

- **Réduction des coûts** : Plus un bug est détecté tard dans le cycle de développement, plus son coût de correction est élevé (il est de coutume de dire qu'un bug coute 100X plus à fixer en production que durant la phase de conception, aucune étude n'a validé cette donnée, mais celà donne un ordre de grandeur plausible).
- **Qualité et fiabilité** : Des tests rigoureux garantissent un niveau de qualité et de fiabilité élevé de l'application.
- **Réputation et satisfaction client** : Les applications stables et fiables renforcent la confiance des utilisateurs.
- **Maintenance facilitée** : Une application bien testée est généralement plus facile à maintenir et à faire évoluer.

> 💡: Le Consortium for Information & Software Quality, a estimé le coût des problèmes de qualité logicielle aux États-Unis en 2022 à environ 2,41 milliards.[^1]

### Niveaux de tests

Les niveaux de tests définissent à quel moment du cycle de développement les tests sont effectués et sur quelle partie du système ils se concentrent.
![Niveaux de tests](@@/pyramid-test.png)

### Types de tests

Les types de tests se concentrent sur les différents aspects de la qualité du logiciel.
![Types de tests](@@/test-tree.png)

#### Tests fonctionnels
##### Tests unitaires

Ces tests se concentrent sur les plus petites unités testables d'un logiciel (fonctions, méthodes, classes). Ils sont généralement écrits par les développeurs.

```javascript
// Exemple de test unitaire avec Jest
test('additionne 1 + 2 pour obtenir 3', () => {
  expect(somme(1, 2)).toBe(3);
});
```

##### Tests d'intégration

Ces tests vérifient que différents modules ou services fonctionnent bien ensemble. Ils peuvent être utiles pour identifier les problèmes d'interface entre les composants.
> 💡: En 1996, le vol inogural lu lanceur Ariane 5 a été un echec. Un bug venant d'une mauvaise intégration d'un code provenant du projet Arianne 4 en est la principale cause.[^2]

##### Tests système ou tests e2e

Ces tests valident le système complet par rapport aux exigences spécifiées. Ils vérifient que le système répond correctement aux besoins des utilisateurs.

##### Tests d'acceptation

Ces tests déterminent si le système satisfait les critères d'acceptation et permettent aux utilisateurs de décider s'ils acceptent le système.


Ils vérifient que chaque fonction du logiciel fonctionne conformément aux spécifications.


#### Tests non-fonctionnels

Ils évaluent les aspects du système qui ne sont pas directement liés aux fonctionnalités spécifiques.

- Performance
- Sécurité
- Utilisabilité
- Compatibilité

## Le cycle de vie des tests

Le cycle de vie des tests décrit les différentes phases du processus de test, de la planification à la clôture.
Ce cycle de vie dépend entre autres du cycle de développement:

![Tests dans le cycle en V](@@/cycle-v.png)
Dans un projet géré en cycle en V, les tests sont créés en parralèles des diférentes phases décendentes

![Tests en agile](@@/agile.png)
Dans un projet géré en agile, on créé les tests pour chaque cycle de développement


### Planification

La planification des tests définit la stratégie, la portée, les objectifs, le calendrier et les ressources nécessaires.

**Éléments clés de la planification :**
- Identification des objectifs de test
- Détermination de la portée et des limites
- Établissement du calendrier et des jalons
- Identification des ressources nécessaires
- Évaluation des risques

### Conception

La phase de conception consiste à créer les cas de tests détaillés et les procédures d'exécution dans un cahier ou plan de tests.

**Activités de conception :**
- Analyse des exigences
- Élaboration des cas de tests
- Définition des données de test
- Préparation de l'environnement de test

### Implémentation et exécution

Cette phase consiste à mettre en place l'environnement de test et à exécuter les tests conformément au plan établi.
Ces tests peuvent être executés manuellement en suivant le cahier de tests ou bien implémentés dans un framework de test.

**Étapes principales :**
- Configuration de l'environnement
- Exécution des tests
- Enregistrement des résultats
- Comparaison des résultats obtenus avec les résultats attendus

### Évaluation et reporting

L'évaluation consiste à analyser les résultats des tests et à communiquer ces informations aux parties prenantes.

**Éléments du reporting :**
- Synthèse des tests effectués
- Anomalies détectées
- Statut d'avancement
- Métriques de qualité
- Recommandations

### Clôture

La clôture des tests comprend l'archivage des éléments de test, l'analyse des leçons apprises et l'évaluation du processus de test.

**Activités de clôture :**
- Vérification de la complétion des tests
- Archivage des éléments de test
- Analyse des leçons apprises
- Amélioration du processus pour les futurs projets

## Élaboration d'un plan de tests

Un plan de tests est un document qui décrit l'approche, les ressources et le calendrier des activités de test. C'est un document essentiel qui sert de feuille de route pour l'équipe de test.

### Structure et composants

Un plan de tests complet comprend généralement les sections suivantes :

1. **Introduction**
    - Objectifs et portée du plan
    - Documents de référence
    - Terminologie

2. **Éléments à tester**
    - Fonctionnalités à tester
    - Fonctionnalités à ne pas tester
    - Priorités de test

3. **Approche de test**
    - Stratégie globale
    - Niveaux de tests à réaliser
    - Techniques de test à utiliser
    - Critères de test

4. **Calendrier**
    - Activités et jalons
    - Dépendances
    - Estimation des efforts

5. **Processus**
    - Procédures d'exécution
    - Gestion des anomalies
    - Suivi et contrôle

```markdown
# Exemple de structure de plan de tests

## 1. Introduction
   - Objectif : Valider les fonctionnalités de l'application de gestion de contacts
   - Portée : Version 2.0 de l'application, interfaces d'administration
   - Références : Spécifications v2.0, User Stories #12-34

## 2. Éléments à tester
   - Création, modification et suppression de contacts
   - Recherche et filtrage des contacts
   - Import/export de données
   
   Hors scope :
   - Module de facturation (prévu v3.0)
   - Synchronisation avec appareils mobiles

## 3. Approche de test
   - Tests unitaires pour chaque composant
   - Tests d'intégration pour les flux principaux
   - Tests de performance pour la recherche
```

### Critères d'entrée et de sortie

Les critères d'entrée et de sortie définissent les conditions qui doivent être remplies pour commencer et terminer les tests.

**Exemples de critères d'entrée :**
- Environnement de test configuré et disponible
- Données de test préparées
- Code prêt pour le test (build stable)
- Exigences revues et approuvées
- Plan de tests approuvé

**Exemples de critères de sortie :**
- Tous les tests planifiés ont été exécutés
- Aucun défaut critique ou majeur non résolu
- Couverture de code minimale atteinte (ex: 80%)
- Documentation complétée et approuvée
- Critères d'acceptation client satisfaits

### Environnement de test

L'environnement de test doit être représentatif de l'environnement de production, tout en permettant l'exécution des tests de manière contrôlée.

**Considérations pour l'environnement de test :**
- Configuration matérielle et logicielle
- Versions spécifiques des composants
- Données de test
- Outils de test nécessaires
- Accès et permissions
- Sauvegarde et restauration

## Techniques de conception des cas de tests

Les techniques de conception des cas de tests aident à créer des tests efficaces et à maximiser la couverture avec un nombre raisonnable de tests.

### Tests boîte noire vs boîte blanche

**Tests boîte noire (Black-box testing)**
- Se concentrent sur les entrées et les sorties
- Ne prennent pas en compte la structure interne du code
- Basés uniquement sur les spécifications et les exigences
- Exemples : tests fonctionnels, tests d'acceptation

**Tests boîte blanche (White-box testing)**
- Examinent la structure interne et la logique du code
- Nécessitent une connaissance de l'implémentation
- Visent à couvrir tous les chemins d'exécution possibles
- Exemples : tests unitaires, tests de couverture de code

**Tests boîte grise (Grey-box testing)**
- Combinaison des approches boîte noire et boîte blanche
- Connaissance partielle de la structure interne
- Permettent des tests plus précis tout en se concentrant sur les comportements
- Exemples : tests d'intégration, tests de sécurité

### Partitionnement en classes d'équivalence

Cette technique consiste à diviser les données d'entrée en groupes (classes) qui devraient être traités de manière similaire par le système.

**Exemple pour une fonction validant l'âge (18-65 ans) :**

| Classe | Description | Exemples | Résultat attendu |
|--------|-------------|----------|------------------|
| C1 | Âge inférieur à 18 | 5, 17 | Rejeté |
| C2 | Âge entre 18 et 65 | 18, 30, 65 | Accepté |
| C3 | Âge supérieur à 65 | 66, 80 | Rejeté |
| C4 | Valeurs non numériques | "abc", "" | Erreur |
| C5 | Nombres négatifs | -5, -20 | Erreur |

L'avantage de cette technique est qu'elle permet de réduire le nombre de tests tout en maintenant une bonne couverture.

### Analyse des valeurs limites

Cette technique se concentre sur les valeurs aux limites des classes d'équivalence, car c'est souvent là que se trouvent les bugs.

**Pour l'exemple précédent (âge 18-65) :**

| Test | Valeur | Description | Résultat attendu |
|------|--------|-------------|------------------|
| T1 | 17 | Juste en dessous de la limite inférieure | Rejeté |
| T2 | 18 | À la limite inférieure | Accepté |
| T3 | 19 | Juste au-dessus de la limite inférieure | Accepté |
| T4 | 64 | Juste en dessous de la limite supérieure | Accepté |
| T5 | 65 | À la limite supérieure | Accepté |
| T6 | 66 | Juste au-dessus de la limite supérieure | Rejeté |

### Table de décision

Les tables de décision sont utiles pour tester des combinaisons de conditions et leurs résultats attendus.

**Exemple pour une fonction d'éligibilité à un prêt :**

| Conditions / Actions | Cas 1 | Cas 2 | Cas 3 | Cas 4 |
|----------------------|-------|-------|-------|-------|
| Revenu > 30000€ | Oui | Oui | Non | Non |
| Score crédit > 700 | Oui | Non | Oui | Non |
| **Prêt accordé** | **Oui** | **Non** | **Non** | **Non** |
| **Taux d'intérêt** | **3%** | **-** | **-** | **-** |

### Tests basés sur les scénarios utilisateurs

Cette approche consiste à créer des cas de tests basés sur des scénarios réels d'utilisation de l'application.

**Exemple pour une application e-commerce :**

**Scénario : Achat d'un produit**
1. L'utilisateur se connecte à son compte
2. L'utilisateur recherche un produit
3. L'utilisateur ajoute le produit au panier
4. L'utilisateur procède au paiement
5. L'utilisateur choisit l'adresse de livraison
6. L'utilisateur effectue le paiement
7. L'utilisateur reçoit une confirmation de commande

Pour chaque étape du scénario, nous pouvons définir des cas de tests spécifiques, y compris les cas de succès et d'échec.

## Tests unitaires

Les tests unitaires sont le premier niveau de tests dans la pyramide de tests. Ils se concentrent sur les plus petites unités testables du logiciel.

### Principes et bonnes pratiques

Les tests unitaires efficaces suivent généralement les principes **FIRST** :
- **F**ast : Les tests doivent s'exécuter rapidement
- **I**solated : Les tests doivent être indépendants les uns des autres
- **R**epeatable : Les tests doivent donner les mêmes résultats à chaque exécution
- **S**elf-validating : Les tests doivent déterminer automatiquement si le résultat est un succès ou un échec
- **T**imely : Les tests doivent être écrits au bon moment (idéalement avant le code de production dans le cadre du TDD)

**Bonnes pratiques :**
- Tester un seul concept par test
- Utiliser des noms de tests descriptifs
- Suivre le pattern "Arrange-Act-Assert" ou "Given-When-Then"
- Maintenir des tests simples et lisibles
- Éviter la logique conditionnelle dans les tests
- Isoler les dépendances externes (base de données, API, etc.)

### Framework de test

La majorité des langages de programmation et framework possèdent des frameworks de tests
- **PHP**: PHPUnit, Pest, Selenium..
- **Ruby**: Capybara, Cucumber, RSpec...
- **JavaScript**: Cypress, Jest, Mocha, Jasmine....
- **Symfony**: panther


**Exemple de test unitaire en javascript avec Jest :**

```javascript
// Fonction à tester
function calculerPrix(quantite, prixUnitaire, reduction = 0) {
  if (quantite <= 0 || prixUnitaire <= 0) {
    throw new Error('La quantité et le prix unitaire doivent être positifs');
  }
  
  let prixTotal = quantite * prixUnitaire;
  
  if (reduction > 0) {
    prixTotal -= prixTotal * (reduction / 100);
  }
  
  return prixTotal;
}

// Tests unitaires
describe('calculerPrix', () => {
  test('calcule correctement le prix sans réduction', () => {
    // Arrange
    const quantite = 5;
    const prixUnitaire = 10;
    
    // Act
    const resultat = calculerPrix(quantite, prixUnitaire);
    
    // Assert
    expect(resultat).toBe(50);
  });
  
  test('calcule correctement le prix avec réduction', () => {
    const resultat = calculerPrix(5, 10, 20);
    expect(resultat).toBe(40);
  });
  
  test('lance une erreur si la quantité est négative', () => {
    expect(() => calculerPrix(-1, 10)).toThrow();
  });
  
  test('lance une erreur si le prix unitaire est null', () => {
    expect(() => calculerPrix(5, 0)).toThrow();
  });
});
```

### TDD (Test-Driven Development)

Le TDD est une approche de développement qui consiste à écrire les tests avant le code de production.

**Cycle TDD (Red-Green-Refactor) :**
1. **Red** : Écrire un test qui échoue
2. **Green** : Écrire le code minimal pour faire passer le test
3. **Refactor** : Améliorer le code sans changer son comportement

**Avantages du TDD :**
- Meilleure compréhension des exigences
- Code plus modulaire et testable
- Moins de bugs et de régressions
- Documentation vivante du comportement attendu
- Feedback rapide

**Exemple de cycle TDD :**

1. **Red** : Écrire un test pour une fonction de validation d'email qui n'existe pas encore
   ```javascript
   test('valide un email correct', () => {
     expect(validerEmail('utilisateur@domaine.com')).toBe(true);
   });
   ```

2. **Green** : Implémenter la fonction minimale pour faire passer le test
   ```javascript
   function validerEmail(email) {
    if (typeof email !== 'string') {
    return false;
    }
    
    const partiesEmail = email.split('@');
    if (partiesEmail.length !== 2) {
    return false;
    }
    
    const [partieDomaine, partieLocale] = partiesEmail;
    
    if (partieDomaine.length === 0 || partieLocale.length === 0) {
    return false;
    }
    
    const partiesDomaine = partieLocale.split('.');
    if (partiesDomaine.length < 2) {
    return false;
    }
    
    const extension = partiesDomaine[partiesDomaine.length - 1];
    if (extension.length < 2) {
    return false;
    }
    
    const caractèresAutorises = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789.-_@";
    for (let i = 0; i < email.length; i++) {
    if (!caractèresAutorises.includes(email[i])) {
    return false;
    }
    }
    
    return true;
    }
   ```

3. **Refactor** : Améliorer l'implémentation
   ```javascript
   function validerEmail(email) {
     const regex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
     return regex.test(email);
   }
   ```

4. Ajouter d'autres tests (Red) et continuer le cycle

## Tests d'intégration

Les tests d'intégration vérifient que différents modules ou services fonctionnent correctement ensemble.

### Intégration des composants

Il existe plusieurs approches pour l'intégration des composants :

- **Intégration big-bang** : Tous les composants sont intégrés simultanément
- **Intégration ascendante (bottom-up)** : Les composants de bas niveau sont testés en premier, puis intégrés aux composants de niveau supérieur
- **Intégration descendante (top-down)** : Les composants de haut niveau sont testés en premier, en utilisant des stubs pour les composants de niveau inférieur
- **Intégration sandwich** : Combinaison des approches ascendante et descendante

### Tests d'API

Les tests d'API sont une forme importante de tests d'intégration dans les applications web modernes.

**Types de tests d'API :**
- Tests de validation des contrats
- Tests de fonctionnalité
- Tests de sécurité
- Tests de performance

**Exemple de test d'API REST avec Jest et Supertest :**

```javascript
const request = require('supertest');
const app = require('../app');

describe('API Utilisateurs', () => {
  test('GET /api/utilisateurs retourne la liste des utilisateurs', async () => {
    const response = await request(app)
      .get('/api/utilisateurs')
      .set('Accept', 'application/json');
    
    expect(response.status).toBe(200);
    expect(response.body).toBeInstanceOf(Array);
  });
  
  test('POST /api/utilisateurs crée un nouvel utilisateur', async () => {
    const nouvelUtilisateur = {
      nom: 'Dupont',
      prenom: 'Jean',
      email: 'jean.dupont@exemple.com'
    };
    
    const response = await request(app)
      .post('/api/utilisateurs')
      .send(nouvelUtilisateur)
      .set('Accept', 'application/json');
    
    expect(response.status).toBe(201);
    expect(response.body).toHaveProperty('id');
    expect(response.body.nom).toBe('Dupont');
  });
});
```

### Mocking 

Le mocking est une technique qui permet d'isoler le code testé en remplaçant ses dépendances par des objets simulés.
Un Mock est un morceau de code venant remplacer un vrai composant, existant ou qui sera développé par la suite.
Il se comporte comme le composant réel, mais sans réellement réaliser les actions (ex: appel à une bdd, à une api, manipulation de fichiers, envoie de mail...) 

**Exemple avec Jest :**

```javascript
// Module à tester
const userService = {
  getUser: async (id) => {
    const userData = await database.fetchUser(id);
    return userData;
  }
};

// Test avec mock
jest.mock('../database');
const database = require('../database');

describe('userService', () => {
  test('getUser retourne les données utilisateur depuis la base de données', async () => {
    // Arrange
    const mockUser = { id: 1, name: 'John' };
    database.fetchUser.mockResolvedValue(mockUser);
    
    // Act
    const result = await userService.getUser(1);
    
    // Assert
    expect(database.fetchUser).toHaveBeenCalledWith(1);
    expect(result).toEqual(mockUser);
  });
});
```

## Tests de bout en bout (E2E)

Les tests E2E vérifient le fonctionnement de l'application de bout en bout, du point de vue de l'utilisateur final.

### Principes et challenges

**Principes :**
- Tester l'application dans un environnement aussi proche que possible de la production
- Simuler les interactions utilisateur réelles
- Vérifier les flux complets de l'application

**Challenges :**
- Tests plus lents à exécuter
- Plus fragiles et susceptibles aux faux échecs
- Configurations plus complexes
- Maintenance plus coûteuse

**Bonnes pratiques :**
- Limiter le nombre de tests E2E aux parcours critiques
- Utiliser des identifiants stables pour les éléments UI
- Utiliser des timeouts appropriés
- Isoler les tests les uns des autres

### Outils populaires

**Cypress**

Cypress est un outil moderne de test E2E qui s'exécute directement dans le navigateur.

```javascript
// Exemple de test Cypress
describe('Application de tâches', () => {
  beforeEach(() => {
    cy.visit('/');
  });
  
  it('permet d\'ajouter une nouvelle tâche', () => {
    // Arrange
    const newTask = 'Nouvelle tâche de test';
    
    // Act
    cy.get('[data-testid="new-task-input"]').type(newTask);
    cy.get('[data-testid="add-task-button"]').click();
    
    // Assert
    cy.get('[data-testid="task-list"]')
      .should('contain', newTask);
  });
  
  it('permet de marquer une tâche comme terminée', () => {
    // Arrange
    cy.get('[data-testid="new-task-input"]').type('Tâche à terminer');
    cy.get('[data-testid="add-task-button"]').click();
    
    // Act
    cy.get('[data-testid="task-checkbox"]').first().click();
    
    // Assert
    cy.get('[data-testid="task-item"]').first()
      .should('have.class', 'completed');
  });
});
```

**Selenium**

Selenium est un framework de test plus ancien mais très puissant pour l'automatisation des navigateurs.

```javascript
// Exemple avec Selenium WebDriver et Node.js
const { Builder, By, until } = require('selenium-webdriver');

describe('Application de tâches', function() {
  let driver;
  
  beforeEach(async function() {
    driver = await new Builder().forBrowser('chrome').build();
    await driver.get('http://localhost:3000');
  });
  
  afterEach(async function() {
    await driver.quit();
  });
  
  it('permet d\'ajouter une nouvelle tâche', async function() {
    const newTask = 'Nouvelle tâche de test';
    
    await driver.findElement(By.css('[data-testid="new-task-input"]')).sendKeys(newTask);
    await driver.findElement(By.css('[data-testid="add-task-button"]')).click();
    
    const taskList = await driver.findElement(By.css('[data-testid="task-list"]'));
    await driver.wait(until.elementTextContains(taskList, newTask), 5000);
  });
});
```

## Tests non-fonctionnels

Les tests non-fonctionnels évaluent les aspects de qualité du système qui ne sont pas directement liés aux fonctionnalités spécifiques.

### Tests de performance

Les tests de performance mesurent la capacité du système à répondre dans différentes conditions.

**Types de tests de performance :**

- **Tests de charge (Load Testing)** : Évaluent le comportement du système sous une charge attendue
- **Tests de stress** : Évaluent le comportement du système sous une charge extrême
- **Tests d'endurance (Soak Testing)** : Évaluent le comportement du système sur une longue période
- **Tests de pointe (Spike Testing)** : Évaluent la réaction du système à des augmentations soudaines de charge

**Outils populaires :**
- JMeter
- k6
- Gatling
- Lighthouse (pour les performances web)

### Tests de Sécurité / Pentest

Les tests de sécurité mesurent la protection du système à une attaque comme vol ou une suppression de données, une interuption de service.





## Bibliographie
- [^1]: [Cost of Poor Software Quality in the U.S.: A 2022 Report](https://www.it-cisq.org/the-cost-of-poor-quality-software-in-the-us-a-2022-report/)
- [^2]: [Conclusions sur l'incident du vol 501 d'Ariane 5](https://fr.wikipedia.org/wiki/Vol_501_d'Ariane_5#Conclusions)
- [Medium: Developing a Test Plan: A Complete Guide](https://medium.com/codex/developing-a-test-plan-a-complete-guide-be3ee94b37df)
- [Openclassroom: Initiez-vous au test et à la qualité logiciel](https://openclassrooms.com/fr/courses/7365096-initiatiez-vous-au-test-et-a-la-qualite-logiciel)
- [Geeksforgeeks: Test Plan Template](https://www.geeksforgeeks.org/test-plan-template/)
- [Documentation de Jest](https://jestjs.io/)
- [Documentation de Cypress](https://www.cypress.io/)
