---
title: Documentation de l'API Quizz
---

## demo

https://quizz.dunarr.com

## Base URL

`https://quizz-api.dunarr.com`

## Authentification

Toutes les routes nécessitant une authentification doivent inclure un header `x-api-key` contenant le token d'authentification de l'utilisateur.

## Routes

### 1. Création de Compte
- **URL**: `/api/account`
- **Méthode**: `POST`
- **Description**: Crée un nouveau compte utilisateur avec un nom d'utilisateur aléatoire et un token
- **Réponse de succès**:
  ```json
  {
    "token": "chaîne_de_32_caractères_hexadécimaux",
    "username": "nom_utilisateur_aléatoire",
    "id": "identifiant_utilisateur",
    "avatar": "url_avatar_généré"
  }
  ```

### 2. Récupération des Informations du Compte
- **URL**: `/api/account`
- **Méthode**: `GET`
- **Authentification**: Requise
- **Description**: Récupère les informations du compte utilisateur connecté
- **Réponse de succès**:
  ```json
  {
    "username": "nom_utilisateur",
    "id": "identifiant_utilisateur",
    "avatar": "url_avatar_généré"
  }
  ```

### 3. Récupération du Quizz
- **URL**: `/api/quizz`
- **Méthode**: `GET`
- **Description**: Récupère la question active du quizz ou initialise un nouveau quizz
- **Réponse de succès**:
  ```json
  {
    "quizz": "identifiant_du_quizz",
    "question_max": "nombre_total_de_questions",
    "remaining_time": "temps_restant",
    "question": null | {
      "num": "numéro_de_question",
      "question": "texte_de_la_question",
      "response1": "première_réponse",
      "response2": "deuxième_réponse",
      "response3": "troisième_réponse",
      "response4": "quatrième_réponse"
    }
  }
  ```
- **Remarque**: Le paramètre question peut être null si le quizz actuel est terminé (toutes les questions ont été posées et le prochain quizz n'est pas encore lancé)

### 4. Envoi de Réponse
- **URL**: `/api/answer`
- **Méthode**: `POST`
- **Authentification**: Requise
- **Description**: Soumet la réponse de l'utilisateur pour la question active
- **Corps de la requête**:
  ```json
  {
    "answer": 1-4
  }
  ```
- **Réponse de succès**:
  ```json
  {
  "success": true,
  "correct": "booléen_indiquant_si_la_réponse_est_correcte",
  "good_answer": "numéro_de_la_bonne_réponse"
  }
  ```

### 5. Récupération des Statistiques
- **URL**: `/api/stats`
- **Méthode**: `GET`
- **Description**: Récupère les statistiques du quizz en cours
- **Réponse de succès**: Liste de statistiques par utilisateur
  ```json
  [
    {
      "username": "nom_utilisateur",
      "avatar": "url_avatar_généré",
      "score": "score_actuel"
    }
  ]


### 6. Obtenir ses Statistiques Personnelles

- **URL**: `/api/my-stats`
- **Méthode**: `GET`
- **Authentification**: Requise
- **Description**: Récupère les statistiques personnelles de l'utilisateur (réponses correctes et incorrectes totales sur tout les quizz) 
- **En-tête requis**: `x-api-key` avec le token d'authentification
- **Réponse de succès**:
  ```json
  {
    "good_answers": "nombre_de_réponses_correctes",
    "bad_answers": "nombre_de_réponses_incorrectes"
  }
  ```
  ```

## Gestion des erreurs
- `400 Bad Request`: Paramètres de requête invalides
- `401 Unauthorized`: Token d'authentification manquant ou invalide

en cas de code 400 un message d'erreur détaillé est retourné dans un paramètre `error" : 
```json
{
  "error": "Message explicatif de l'erreur"
}
```