---
title: Cookies & sessions en PHP
---
## Présentation
Dans un site web, on peut souhaiter sauvegarder des informations durablement sur la navigation de l'utilisateur,   
par ex : 
 - connexion à un compte
 - pannier
 - préférence de couleur / thème sombre
 - historique des actions
 - etc

Pour sauvegarder ces informations, il existe plusieurs technologies, les principales sont :
 - [les cookies](#les-cookies)
 - le local storage[^1]
 - les sessions (js)[^2]
 - [les sessions](#les-sessions) (serveur)

Il en existe d'autres qui ont des usages plus spécifiques :
 - le cache
 - l'indexDB

## Les cookies
Les cookies peuvent être créés côté serveur (php) ou client (js)   
Ils sont sauvegardés dans le navigateur du client et sont envoyés au serveur à chaque requête.   
Le navigateur les envoie au serveur automatiquement dans les headers de chaque re requête.   
Si le cookie est créé côté serveur, l'instruction de création est envoyé au navigateur dans un des headers de la réponse.   
Les cookies ont une date d'expiration, passé cette date, ils disparaissent automatiquement.
Avant cela, ils restent dans le navigateur même s'il est fermé ou l'ordinateur est redémarré.

Un cookie est restraint à un nom de domaine, il sera accessible à toutes les pages sur ce domaine, mais pas ailleurs.
par example, un cookie créé sur facebook.com sera accessible à toutes les pages du site, mais pas sur google.com. 


### Création de cookie (en php)
```php
<?php
setcookie("first_name", "William", time() + 3600);
```
Pour créer un cookie, on doit utiliser [fonction setcookie](https://www.php.net/manual/en/function.setcookie.php).   
Elle prend minimum 3 paramètres :
 - le nom du cookie à créer
 - la valeur à stocker (string, number, boolean)
 - la date d'expiration du cookie (un timestamp[^3])

Pour la date d'expiration, on peut utiliser la fonction `time()` qui nous donne le timestamp actuel
et ajouter le nombre de secondes que l'on souhaite garder le cookie.   
Par ex: `time()+3600` donne une durée de vie d'une heure.


### Lecture de cookie (en php)
```php
<?php
echo "Bonjour " . $_COOKIE["first_name"];
```
Pour lire un cookie, on utilise la variable serveur `$_COOKIE`.
C'est un tableau associatif accessible dans tout le programme php qui contient tout les cookies du site.
On peut accéder aux valeurs à l'intérieur de la même manière que dans un tableau.

/!\ on ne peut pas modifier le contenu de cette variable directement /!\ 

## Les sessions
Les sessions PHP permettent de stocker des informations de la même manière que des cookies, mais celles-ci sont stockées sur le serveur.
Ainsi, l'utilisateur ne peut pas les voire, il ne peut donc pas les modifier ou les falsifier,
cela permet par example de gérer une connexion utilisateur. 
Enfin, la session d'un utilisateur disparait lorsque celui-ci ferme son navigateur.

Pour manipuler la session, il suffit d'initialiser celle-ci avec la fonction `session_start`
puis on peut lire et modifier la session avec la variable `$_SESSION`

### Création de session
```php
<?php
session_start();
$_SESSION["first_name"] = "William";
$_SESSION["last_name"] = "Lefebvre";
```

### Lecture de session
```php
<?php
session_start();
echo "Bonjour " . $_SESSION["first_name"]." ".$_SESSION["last_name"];
```


## Notes
[^1]: Le local storage est un stockage accessible uniquement en js, il permet de stocker indéfiniment des informations dans le navigateur 

[^2]: La session (en JS) est l'équivalent de la session PHP, elle disparait à la fermeture du navigateur et n'est accessible qu'au js

[^3]: un timestamp est le nombre de secondes écoulées depuis le 1er janvier 1970 (donc un nombre entier), 
il s'agit s'un système de mesure du temps régulièrement utilisé en programmation.