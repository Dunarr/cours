---
title: Formulaire HTML
next: "@/php/form/php"
---
## Intro
Un formulaire HTML permet à l'utilisateur de renseigner des informations sur une interface web et de les envoyer au serveur 
(ex: formulaire d'inscription / connexion, formulaire de commetaire, backoffice...).

Pour créer un formulaire, il faut créer la partie html, que l'utilisateur utilisera,   
ainsi que la partie php qui recevra et traitera les données (voir [chapitre suivant](@/php/form/php))

Nous verrons ici les differents composants permettant de construire le formulaire HTML
## Bases d'un formulaire:

```HTML
<form> <!--balise form qui eveloppe le formulaire-->
    <input type="text" name="message"> <!--inputs(champs) du formulaire-->
    <input type="submit"> <!--Boutton valider pour envoyer le formulaire-->
</form>
```

## Balise `<form>`

```HTML
<form action="traitement.php" method="post" enctype="multipart/form-data"></form>
```

La balise form est la base du formulaire, elle définiele comportement général du forulaire à l'envoi.
On peut la configurer avec differents attributs, les principaux sont:

| attribut | description                                                       | valeurs                                         |
|----------|-------------------------------------------------------------------|-------------------------------------------------|
| action   | fichier / url ou sera envoyé le formulaire (vide si même fichier) | `/traitement.php`,                              |
|          |                                                                   | `https://example.com/traitement.php`            |
| method   | methode HTTP utilisée pour envoyer le formulaire (get par défaut) | `get` => données dans l'url                     |
|          |                                                                   | `post` => données dans le contenu de la requête |
| enctype  | encodage du formulaire (utilisé pour l'envoi de piece jointe)     | `multipart/form-data`                           |

## Inputs

Il existe 3 balise pour faire des champs de formulaire

- `<input type="text" name="message">`
  => inputs de base, il en existe plusieurs types en fonction de ce que l'on doit mettre à l'interieur
- `<textarea name="message></textarea>`
  => zones de texte, pour rentrer des textes plus longs avec des sauts de ligne.
- `<select name="message"><option value="choix-1">Choix 1</option></select>`
  => liste déroulante, pour proposer des choix à l'utilisateur

l'attribut `name` définie le nom de l'input, il sera récupérer la valeur entrée par l'utilisateur.
Comme un Id, il doit être unique (sauf pour des inputs multiples, voir chapitre sur les inputs multiples) et ne doit être composé que de chiffres/lettres/tirets.


### Balise `<input>`

Il y à plusieurs types d'inputs avec des fonctionnalités différentes, vous pouvez les retouver sur [mdn](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/Input#input_types)
le type d'input peut se choisir avec l'attribut `type` 

example: 
```HTML
<input type="color">
```

Le comportement des inputs peuvent aussi varier avec des attributs supplémentaires (globaux, ou spécifiques à cerains types), vous povez en retrouver la liste sur [mdn](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/Input#attributes)

example:
```HTML
<input type="number" min="10" max="20" step="0.5" required>
```

La valeur par défaut des inputs est modifiable avec l'attribut `value`, cela permet aussi de préremplir un champ en PHP

examples:

```HTML
<input type="text" name="content" value="Valeur par défaut">

<input type="text" name="content" value="<?= $content ?>">
```

pour que le formulaire, puisse envoyer, il faut ajouter un input de type `submit` qui est un boutton d'envoi. Le texte du boutton est editable avec l'attribut `value`

example:
```HTML
<form action="">
  <input type="submit" value="Envoyer">
</form>
```

### Balise `<textarea>`

Un textarea est une zone de texte adapté pour la redaction de contenu plus long qu'un input. Il s'utilise comme un input et doit donc avoir un attribut name.

example:
```html
<form action="">
  <textarea name="content"></textarea>
  <input type="submit">
</form>
```

La valeur par défaut est définie entre la balise ouvrante et fermante du textarea

example:
```HTML
  <textarea name="content" >Ce texte est écrit par défaut dans la zone de texte</textarea>
```

### Balise `<select>`

Le select est un dropdown permetant de à l'utilisateur de choisir une valeur parmis celles proposées.   
Le select à un attribut `name` comme un input.  
La balise `option` permet de définir les valeur possibles du select. les ballises option sont dans la balise select.  
Chaque option représente un choix, sa valeur est définie avec l'attribut value et le texte affiché est écrit à l'interieur de la balise  

example:
```HTML
<select name="order">
  <option value="price_asc">Prix croissant</option>
  <option value="price_desc">Prix décroissant</option>
  <option value="name_asc">Alphabétique</option>
</select>
```
Dans chaque option, il y à un label (dans la balise) et une valeur (attribut `value`), le label est affiché à l'utilisateur et la valeur est envoyée au serveur si l'option est séléctionnée
example: si on choisi l'option "Prix croissant", alors "price_asc" sera envoyé au serveur comme valeur de "order"


## Upload de fichiers

Un formulaire permet d'envoyer des pièces jointes.
Pour cela, il faut utiliser un input de type `file`.

**afin d'uploader un fichier, il faut ajouter l'attribut `enctype="multipart/form-data"` sur la balise form**

example:
```HTML
<form action="" enctype="multipart/form-data">
  <input type="file" name="mon-fichier">
  <input type="submit">
</form>
```

## Balise `<label>`

Affin d'indiquer la description des champs à l'utilisateur, il faut utiliser une balise `label`.   
La description du champ est renseignée dans le contenu de la balise de la même manière qu'une balise `div`.   
Un label doit être lié à un input, il existe pur cela deux solutions.   

Mettre l'input dans le label:
```HTML
<label>
  Prénom
  <input type="text" name="first_name">
</label>
```

Utiliser l'attribut `for`, il doit être égal à l'id de l'input à lier:
```HTML
<label for="fname_input">Prénom</label>
<input type="text" name="first_name" id="fname_input">
```


