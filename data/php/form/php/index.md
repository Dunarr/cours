---
title: Traitement des données de formulaire en PHP
prev: "@/php/form/html"
---
## Intro
Lorsqu'un utilisateur envoie un formulaire à un script PHP, celui-ci reçoit toutes les informations renseignées sous forme de tableau.
On peut ensuite l'utiliser de plusieurs manières différentes, par exemple :
 - faire un traitement algorithmique
   - calculatrice
   - traitement de texte
   - vérification / validation des informations
 - exécuter une requête en BDD
   - formulaire de recherche
   - formulaire de connexion
 - sauvegarder les données
   - Backoffice
   - formulaire d'inscription

Nous allons voir ici comment récupérer et traiter les données d'un formulaire HTML.


## $_POST et $_GET

En PHP, deux *variables supergloballes* `$_GET` et `$_POST` sont définies automatiquement.
Si un script PHP est appelé à partir d'un formulaire en méthode get, alors, les données renseignées seront dans la variable $_GET.
Et si le formulaire est en méthode post, la variable correspondante est $_POST.
Ces variables contiennent un tableau associatif, les clefs sont les noms (`name`) des champs des inputs du formulaire, les valeurs sont les données entrées par l'utilisateur dans les champs.

À partir de maintenant, les exemples utiliserons un formulaire en méthode post, mais fonctionneraient de la même manière en get.

Exemple de formulaire :
```HTML
<!--index.html-->
<form action="traitement.php" method="post">
    <label for="age-input">Votre âge</label>
    <input type="number" id="age-input" name="age">
    
    <label for="username-input">Nom d'utilisateur</label>
    <input type="text" id="username-input" name="username">
    
    <label for="password-input">Mot de passe</label>
    <input type="password" id="password-input" name="password">
    
    <input type="submit" value="s'inscrire">
</form>
```
```php
var_dump($_POST);
/** Example de résultat
 * array(3) {
 *    ["age"]=>
 *    string(2) "15"  (même si on à un input de type nombre, le résultat sera une chaîne de caractères)
 *    ["username"]=>
 *    string(6) "dunarr"
 *    ["password"]=>
 *    string(13) "mon-super-mdp"
 * }
 *
 *
 * Résultat avec des champs non remplis 
 * (un input vide envoyé donne une chaine de caractères vide)
 * array(3) {
 *    ["age"]=>
 *    string(0) ""
 *    ["username"]=>
 *    string(0) ""
 *    ["password"]=>
 *    string(0) ""
 * }
 */
 
var_dump($_GET);
/** example de résultat 
 * (si la variable $_POST/$_GET n'est pas remplie par un formulaire,
 * elle contient un tableau vide)
 * array(0) {
 * }
 */
```

On peut ainsi récupérer les informations indiquées par l'utilisateur de la même manière que l'on traite un tableau classique.

Par exemple :

```php
$username = $_POST["username"];
```

## Vérification des données d'entrée :

**Il ne faut jamais faire confiance aux données indiquées par un utilisateur.** 
Un utilisateur peut faire des erreurs, voir essayer de pirater notre application, 
il faut donc toujours vérifier les informations qui viennent d'un formulaire.

Un formulaire HTML permet de faire des vérifications automatiques dans le navigateur du client,
mais elles peuvent facilement être contournées. Il faut donc aussi faire une vérification en PHP 

### isset

La première vérification que l'on peut (doit) faire sur un champ de formulaire, est de vérifier qu'il a bien été envoyé.
Ici, on vérifie que l'input a été envoyé, mais on ne détermine pas si le champ a été rempli (input vide != pas d'input).

```php
if(isset($_POST["username"])){
    $username = $_POST["username"];
    // utiliser la variable $username
}
```

La fonction `isset` permet en PHP de vérifier si une clef est définie dans un tableau, elle n'est pas spécifique au traitement de tableaux.
Ce code permet de vérifier que l'input `username` a bien été envoyée.
La condition `isset($_POST["username"])` peut être fausse dans plusieurs cas :
 - le formulaire est sur la même page et n'a pas été envoyé, il faut donc l'afficher
 - un utilisateur a supprimé le champ du formulaire (possible tentative d'hacking)
 - un utilisateur s'est rendu sur cette page involontairement et ne devrait pas y être.
   Il faut alors effectuer une redirection ou afficher un message pour renvoyer l'utilisateur sur la bonne page.

### Input renseigné

Comme vu précédemment, un input peut être envoyé vide par l'utilisateur.
Afin de vérifier qu'un input a bien été renseigné, on peut simplement comparer la valeur reçue à une chaine vide :
```php
// avant de vérifier si $_POST["username"] !== "", on vérifie si l'input à été envoyé.
if(isset($_POST["username"]) && $_POST["username"] !== ""){ 
    $username = $_POST["username"];
    // utiliser la variable $username
}
```

Cette vérification permet par exemple de vérifier si l'utilisateur a bien rempli les champs obligatoires.

### Autres vérifications

Les valeurs reçues sont des chaînes de caractères, on peut ainsi effectuer plusieurs validations sur celles-ci en PHP.
Par exemple :
 - longueur
 - caractères permis
 - comparaison aux données d'une BDD
 - validation d'adresse
 - Vérification d'un intervalle

## Upload de fichiers

