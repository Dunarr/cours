---
title: Manipulation de fichiers texte en PHP
---

## Intro

Php permet de manipuler divers types de fichiers, nativement ou avec des librairies.
Il peut manipuler des fichiers texte sauvegardées dans la machine qui l'éxecute 
(il ne peut pas manipuler les fichiers de la machine cliente).

Les actions de base réalisables avec php sur des fichiers texte sont
 - lire
 - ecrire / modifier
 - lister


Grâce à php, on peut ainsi gérer des fichiers `.txt`, mais aussi tout type de fichier qui contient du texte.

## Lecture de fichier

Php peut lire le contenu d'un fichier grâce à la fonction [file_get_contents](https://www.php.net/manual/en/function.file-get-contents.php).
Cette fonction retourne le contenu du fichier indiqué sous forme d'une chaîne de caractères. les sauts de lignes sont indiqués par un `\n`.
```php
$content = file_get_contents("file.txt"); // fichier dans le même dossier que le script 
$content = file_get_contents("directory/file.txt"); // fichier dans un sous-dossier
$content = file_get_contents("../file.txt"); // fichier dans le dossier parent
$content = file_get_contents("/var/www/file.txt"); // position absolue du fichier sur la machine 
```

La fonction `file_get_contents` doit prendre en paramètre le chemin vers le fichier à lire.
Ce chemin peut être relatif(au script php) ou absolu et prend le même forme que les chemins de l'OS.

On peut aussi utiliser la fonction [file](https://www.php.net/manual/en/function.file.php) afin de lire un fichier.
Celle-ci retourne le fichier sous forme d'un tableau, chaque entrée correspond à une ligne du fichier.

```txt
Salut
tout
le monde
```
```php
$lines = file("file.txt");
var_dump($lines);
/**
 * array(3) {
 *   [0]=>
 *   string(5) "Salut"
 *   [1]=>
 *   string(4) "tout"
 *   [2]=>
 *   string(8) "le monde"
 *}
 */
```

## Écriture de fichier

On peut écrire du texte dans un fichier avec la fonction [file_put_contents](https://www.php.net/manual/en/function.file-put-contents.php)
**Par défaut, cette fonction efface le contenu du fichier avant d'écrire**.

Cette fonction prend 2 paramètres, le chemin vers le fichier et le contenu à écrire. Le contenu doit être une chaîne de caractères.

example:
```php
file_put_contents("file.txt", "Lorem ipsum dolor sit amet");
```

Si l'on souhaite ajouter du contenu à un fichier existant, on doit ajouter le flag `FILE_APPEND` en paramètre.
```php
file_put_contents("file.txt", "Lorem ipsum dolor sit amet", FILE_APPEND);
```

enfin, si le fichier à modifier n'éxiste pas, php le créé automatiquement.

## Suppression de fichier
PHP peut aussi supprimer un fichier du disque. pour celà, on utilise la fonctio [unlink](https://www.php.net/manual/en/function.unlink.php)
on doit lui passer en paramètre le chemin du fichier à supprimer.
exemple:
```php
unlink('dossier/fichier_a_supprimer.txt');
```

## Liste des fichiers
On peut lister les fichiers dans un dossier à l'aide de la fonction [glob](https://www.php.net/manual/en/function.glob.php)
cette fonction prend en paramètre un patterne permetant de filtrer les fichiers à lister.
Vous pouvez retrouver la syntaxe exacte de ces patternes sur la documentation, le cas le plus général est le wildcard (*) qui permet de séléctionner tout,
quelques exemples:
```php
// séléctionne tout les fichiers dans le dossier courrant
glob('*');
// séléctionne tout les fichiers qui finissent par '.txt'
glob('*.txt');
// séléctionne tout les fichiers dans un sous-dossier
glob('dossier/*');
// séléctionne tout les fichiers commençant par 'monfichier'
glob('monfichier*');
// séléctionne tout les fichiers dont le nom contient 'monfichier'
glob('*monfichier*');
```
