---
title: Manipulation de fichiers texte en PHP
---

## fonctions de manpiluation de fichiers

PHP peut accéder au system de fichier **du serveur** (il ne peut pas manipuler les fichiers de la machine cliente). de
la même manière qu'en ligne de commande. Pour cela, on utilise une serie de fonction que l'on peut retrouver
dans [la documentation PHP](https://www.php.net/manual/fr/book.filesystem.php)
par example, pour créer un fichier, on peut utiliser la fonction `touch`, Example:

```php
touch("mon-fichier.txt");
```

Il existe des fonctions pour la plus part des actions que l'on peut faire sur le systeme
(lire / créer / modifier / supprimer / lister des fichiers et dossiers entre autre).

## Droits et serveur web

**En général**, nos scripts php sont executés par notre serveur web (apache / nginx). Ce dernier est lancé par un
utilisateur systeme qui lui est dedié (www-data). Toutes les manipulations des fichiers sont donc faites par www-data,
on doit donc lui donner le droit de le faire. si l'on veut créer / modifier un fichier en PHP, il faut donc changer le
owner ou les droits du dossier/fichier qui doit être manipulé. Par example, sur un sit qui permet d'uploader des fichier
via un formulaire, on créra un dossirer `uplads/` dont l'owner sera www-data, tout les fichiers seront uploadés dedans.

## lecture /ecriture de fichiers

PHP peut lire et modifier le contenu d'un fichier texte. Celà se fait en 3 étapes:

- ouverture du fichier: `fopen`
- modification / lecture du fichier: `fwrite` / `fread`
- fermeture du fichier `fclose`

__Example de modification de fichier__
```php
$file = fopen("fichier.txt", "w");
fwrite($file, "Bonjour");
fclose($file);
```

Ce code écrit "Bonjour" dans le fichier `fichier.txt`


__Example de lecture de fichier__
```php
$file = fopen("fichier.txt", "r");
if(filesize("fichier.txt")){
    $content = fwrite($file, filesize("fichier.txt"));
} else {
    $content = "";
}
echo $content;
fclose($file);
```


__Example d'ajout de texte à un fichier__
```php
$file = fopen("fichier.txt", "a");
fwrite($file, "Bonjour!\n");
fclose($file);
```

Ce code ajoute une ligne avec "Bonjour!" dans le fichier `fichier.txt`
(fwrite ne saute pas de lignes tout seul, il faut en jouter avec `\n`)

### Modes d'ouverture

Pour manipuler le contenu d'un fichier, on doit commencer par l'ouvrir avec `fopen`
cette fonction prend 2 parametres: le nom du fichier et le "mode"
le mode permet de choisir le comportement à adopter avec le fichier. Il existe
plusieurs [modes d'ouvertures](https://www.php.net/manual/fr/function.fopen.php#refsect1-function.fopen-parameters)
voici les principaux:

- **r**: lecture, permet uniquement de lire le contenu d'un fichier
- **w**: ecriture, ecrase le contenu d'un fichier (/!\ le contenu du fichier est instentanemment ecrasé /!\)
- **a**: ajout, permet d'ajouter du contenu à la fin du fichier

on ne pourra pas faire de `fread` si le fichier est ouvert en **a** ou en **w**
on ne pourra pas faire de `fwrite` si le fichier est ouvert en **r**

