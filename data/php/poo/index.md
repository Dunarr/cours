---
title: Programmation Orientée Objet en PHP
---

## **Introduction à la Programmation Orientée Objet (POO)**

### **Qu'est-ce que la POO ?**
La Programmation Orientée Objet (POO) est un paradigme de programmation qui utilise des "objets" pour structurer le code. Un objet est une instance d'une classe, qui peut contenir des données (propriétés) et des fonctions (méthodes).

### **Principaux Concepts de la POO**
- **Classe** : Une classe est un plan ou un modèle pour créer des objets. Elle définit des propriétés (variables) et des méthodes (fonctions) qui seront utilisées par les objets.
- **Objet** : Un objet est une instance d'une classe. C'est un élément concret qui utilise la structure définie par la classe.
- **Encapsulation** : Cacher les détails internes d'une classe et n'exposer que les éléments nécessaires.
- **Héritage** : Permet à une classe d'hériter des propriétés et méthodes d'une autre classe.
- **Polymorphisme** : Permet d'utiliser une même méthode de manière différente selon l'objet.
- **Abstraction** : Créer des classes abstraites pour définir un comportement commun sans donner d'implémentation complète.

---

## **Les Classes et Objets en PHP**

### **Déclaration d'une Classe**
Pour définir une classe en PHP, on utilise le mot-clé `class`. Voici un exemple simple :

```php
class Personne {
    // Propriétés (variables)
    public string $nom;
    public int $age;

    // Méthode (fonction)
    public function sePresenter() {
        echo "Bonjour, je m'appelle " . $this->nom . " et j'ai " . $this->age . " ans.";
    }
}
```

### **Créer un Objet**
Pour créer un objet à partir d'une classe, on utilise le mot-clé `new` :

```php
$personne1 = new Personne();
$personne1->nom = "Alice";
$personne1->age = 25;
$personne1->sePresenter(); // Affiche : Bonjour, je m'appelle Alice et j'ai 25 ans.
```

---

## **Les Propriétés et Méthodes**

### **Propriétés**
Les propriétés sont des variables définies dans une classe. Elles représentent l'état d'un objet. On peut définir la visibilité des propriétés avec les mots-clés :
- `public` : Accessible de l'extérieur.
- `private` : Accessible uniquement dans la classe elle-même.
- `protected` : Accessible dans la classe et ses classes dérivées.

```php
class Voiture {
    public string $marque;
    private int $vitesse;
    
    public function __construct(string $marque, int $vitesse = 0) {
        $this->marque = $marque;
        $this->vitesse = $vitesse;
    }
    
    public function getVitesse(): int {
        return $this->vitesse;
    }
    
    public function accelerer(int $increment): void {
        $this->vitesse += $increment;
    }
}
```

### **Méthodes**
Les méthodes sont des fonctions définies dans une classe. Elles représentent le comportement d'un objet. Pour accéder aux propriétés d'un objet dans une méthode, on utilise `$this->`.

```php
$voiture = new Voiture("Tesla");
$voiture->accelerer(50);
echo $voiture->getVitesse(); // Affiche : 50
```

### **Constructeur**
Le constructeur est une méthode spéciale (`__construct`) qui est appelée automatiquement lors de la création d'un objet.

```php
class Animal {
    public string $espece;

    // Constructeur
    public function __construct(string $espece) {
        $this->espece = $espece;
    }
}
```

---

## **Encapsulation**

### **Accesseurs (Getters) et Mutateurs (Setters)**
L'encapsulation permet de protéger les données en rendant certaines propriétés privées et en utilisant des méthodes pour les manipuler.

```php
class CompteBancaire {
    private float $solde;

    public function __construct(float $soldeInitial) {
        $this->solde = $soldeInitial;
    }

    // Getter
    public function getSolde(): float {
        return $this->solde;
    }

    // Setter
    public function deposer(float $montant): void {
        if ($montant > 0) {
            $this->solde += $montant;
        }
    }
}
```

---

## **Héritage**

### **Définition de l'Héritage**
L'héritage permet de créer une nouvelle classe qui hérite des propriétés et méthodes d'une classe existante. On utilise le mot-clé `extends`.

```php
class Animal {
    protected string $nom;

    public function __construct(string $nom) {
        $this->nom = $nom;
    }

    public function seDeplacer() {
        echo $this->nom . " se déplace.";
    }
}

// Classe enfant
class Chien extends Animal {
    public function aboyer() {
        echo $this->nom . " aboie !";
    }
}
```

### **Utilisation de l'Héritage**

```php
$chien = new Chien("Rex");
$chien->seDeplacer(); // Affiche : Rex se déplace.
$chien->aboyer(); // Affiche : Rex aboie !
```

---

## **Polymorphisme**

### **Méthodes Redéfinies**
Le polymorphisme permet de redéfinir des méthodes héritées d'une classe parent.

```php
class Vehicule {
    public function demarrer() {
        echo "Le véhicule démarre.";
    }
}

class Moto extends Vehicule {
    // Redéfinition de la méthode demarrer
    public function demarrer() {
        echo "La moto démarre en faisant un bruit de moteur.";
    }
}
```

### **Utilisation du Polymorphisme**
On peut utiliser la même méthode sur des objets de types différents.

```php
$vehicule = new Vehicule();
$moto = new Moto();

$vehicule->demarrer(); // Affiche : Le véhicule démarre.
$moto->demarrer(); // Affiche : La moto démarre en faisant un bruit de moteur.
```

---

## **Classes Abstraites et Interfaces**

### **Classes Abstraites**
Une classe abstraite ne peut pas être instanciée directement et peut contenir des méthodes abstraites (sans corps). Elle sert de base pour d'autres classes.

```php
abstract class Forme {
    abstract public function calculerSurface(): float;
}

class Rectangle extends Forme {
    private float $largeur;
    private float $hauteur;

    public function __construct(float $largeur, float $hauteur) {
        $this->largeur = $largeur;
        $this->hauteur = $hauteur;
    }

    public function calculerSurface(): float {
        return $this->largeur * $this->hauteur;
    }
}
```

### **Interfaces**
Une interface définit un contrat. Toutes les classes qui implémentent une interface doivent définir les méthodes spécifiées dans l'interface.

```php
interface Affichable {
    public function afficher(): void;
}

class Article implements Affichable {
    public function afficher(): void {
        echo "Affichage de l'article.";
    }
}
```

---

## **Gestion des Exceptions**

### **Lancer et Attraper des Exceptions**
PHP permet de gérer les erreurs grâce aux exceptions. On utilise `try`, `catch`, et `throw`.

```php
class Division {
    public function diviser(float $a, float $b): float {
        if ($b == 0) {
            throw new Exception("Division par zéro interdite !");
        }
        return $a / $b;
    }
}

try {
    $calcul = new Division();
    echo $calcul->diviser(10, 0);
} catch (Exception $e) {
    echo "Erreur : " . $e->getMessage();
}
```

---

## **Les Bonnes Pratiques en POO**
1. **Utiliser l'encapsulation** pour protéger les données.
2. **Séparer les responsabilités** : chaque classe doit avoir une responsabilité unique.
3. **Nommer les classes, méthodes et propriétés de manière explicite**.

---

## **Exercices de Révision**

1. Créer une classe `Produit` avec des propriétés comme `nom`, `prix` et une méthode pour appliquer une réduction.
2. Créer une classe `Personne` avec des propriétés `nom`, `age` et une méthode pour afficher ces informations. Créer une classe `Etudiant` qui hérite de `Personne` avec des propriétés supplémentaires comme `niveau` et une méthode pour afficher le niveau.
3. Implémenter une interface `Payable` avec une méthode `calculerSalaire`. Créer deux classes `Employé` et `Freelance` qui implémentent cette interface avec des calculs de salaire différents.

