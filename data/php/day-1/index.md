---
title: Bases du PHP
---

## Objectifs du module

- Découvrir le rôle de PHP dans le développement web côté serveur.
- Installer et configurer un environnement de développement PHP.
- Maîtriser les bases de la syntaxe PHP et les structures de contrôle.
- Comprendre l'utilisation et la création de fonctions
- Savoir quand et comment utiliser des tableaux

---

## 1. Introduction à PHP et au Développement Web Côté Serveur

### Présentation de PHP

- **PHP (PHP: Hypertext Preprocessor)** est un langage de programmation conçu pour le développement de sites web dynamiques.
- Il permet de générer dynamiquement des pages web. Il peut aussi être utilisé pour des scripts en ligne de commande
- Il s'agit d'un langage interprété, orienté objet et faiblement typé.
- PHP permet de facilement afficher du contenu en HTML

### Installation de l'environnement de développement

1. Installez WAMP, LAMP, XAMPP ou Docker pour configurer un serveur web local.
2. Installez un éditeur de code (vscode, phpstorm, sublime text)
2. Vérifiez l'installation en exécutant un premier script PHP :

    ```php
    <?php
    echo "Hello, World!";
    ?>
    ```

---

## 2. Premiers Concepts en PHP


### Variables et Types de Données

- Une **variable** est une zone de mémoire dans laquelle on stocke une valeur que l’on souhaite utiliser et manipuler au sein du programme.
- En PHP, les noms de variables commencent toujours par un signe `$` suivi d'un nom significatif.

#### Déclaration de Variables

- Pour déclarer une variable en PHP, il suffit de l’initialiser en utilisant le symbole `$`, suivi d’un nom et d’une valeur. Par exemple :

    ```php
    $nom = "Jean";   // Variable de type chaîne (string)
    $age = 25;       // Variable de type entier (int)
    $prix = 12.99;   // Variable de type décimal (float)
    $estActif = true; // Variable de type booléen (bool)
    ```

#### Types de Données en PHP

Les variables peuvent contenir différents types de données :

1. **String (chaîne de caractères)** : Un ensemble de caractères. Délimité par des guillemets simples `' '` ou doubles `" "`.
   ```php
   $nom = "Jean"; // Chaîne de caractères
   ```

2. **Int (entier)** : Un nombre entier, positif ou négatif.
   ```php
   $age = 25;
   ```

3. **Float (nombre décimal)** : Un nombre avec des décimales.
   ```php
   $prix = 19.99;
   ```

4. **Bool (booléen)** : Valeur vraie ou fausse, utilisée souvent pour les tests et les conditions.
   ```php
   $estConnecte = true;
   ```

5. **Array (tableau)** : Une collection de valeurs, qui peut être de différents types (indexé, associatif, ou multidimensionnel).
   ```php
   $couleurs = ["rouge", "vert", "bleu"];
   ```

---

### Opérateurs

Les opérateurs sont des symboles qui permettent de manipuler les valeurs des variables pour effectuer des calculs, des comparaisons, et d’autres opérations.

#### Opérateurs Arithmétiques et de Concaténation

1. **Addition (`+`)** : Additionne deux nombres.
   ```php
   $resultat = 5 + 3; // $resultat vaut 8
   ```

2. **Soustraction (`-`)** : Soustrait un nombre d'un autre.
   ```php
   $resultat = 10 - 4; // $resultat vaut 6
   ```

3. **Multiplication (`*`)** : Multiplie deux nombres.
   ```php
   $resultat = 4 * 3; // $resultat vaut 12
   ```

4. **Division (`/`)** : Divise un nombre par un autre.
   ```php
   $resultat = 20 / 4; // $resultat vaut 5
   ```

5. **Modulo (`%`)** : Donne le reste d'une division entière.
   ```php
   $resultat = 10 % 3; // $resultat vaut 1
   ```

6. **Concaténation (`.`)** : Utilisé pour combiner des chaînes de caractères.
   ```php
   $prenom = "Jean";
   $nom = "Dupont";
   $nomComplet = $prenom . " " . $nom; // $nomComplet vaut "Jean Dupont"
   ```

#### Opérateurs d'Assignation

Les opérateurs d'assignation permettent d'affecter ou de modifier la valeur d'une variable.

1. **`=`** : Affecte une valeur(en dur, résultat d'une opération ou d'une fonction) à une variable.
   ```php
   $a = 5;
   $b = $a + 1;
   $c = rand(1,3);
   ```

2. **`+=`** : Additionne et assigne la valeur.
   ```php
   $a = 5;
   $a += 3; // $a vaut maintenant 8
   ```

3. **`-=`** : Soustrait et assigne la valeur.
   ```php
   $a = 5;
   $a -= 2; // $a vaut maintenant 3
   ```

4. **`*=`** : Multiplie et assigne la valeur.
   ```php
   $a = 5;
   $a *= 3; // $a vaut maintenant 15
   ```

5. **`/=`** : Divise et assigne la valeur.
   ```php
   $a = 10;
   $a /= 2; // $a vaut maintenant 5
   ```

6. **`%=`** : Applique le modulo et assigne la valeur.
   ```php
   $a = 10;
   $a %= 3; // $a vaut maintenant 1
   ```

7. **`.=`** : Concatène et assigne une chaîne de caractères.
   ```php
   $message = "Bonjour";
   $message .= " tout le monde"; // $message vaut maintenant "Bonjour tout le monde"
   ```

#### Opérateurs d'Incrémentation et de Décrémentation

1. **Incrémentation (`++`)** : Augmente la valeur d'une variable de 1.
   ```php
   $a = 5;
   $a++; // $a vaut maintenant 6
   ```

2. **Décrémentation (`--`)** : Diminue la valeur d'une variable de 1.
   ```php
   $a = 5;
   $a--; // $a vaut maintenant 4
   ```

#### Opérateurs de Comparaison

Les opérateurs de comparaison sont utilisés pour comparer des valeurs et sont souvent employés dans les conditions.

1. **Égalité (`==`)** : Vérifie si deux valeurs sont égales.
   ```php
   $a = 5;
   $b = 5;
   var_dump($a == $b); // Retourne true
   ```

2. **Différence (`!=`)** : Vérifie si deux valeurs sont différentes.
   ```php
   $a = 5;
   $b = 3;
   var_dump($a != $b); // Retourne true
   ```

3. **Identique (`===`)** : Vérifie si deux valeurs sont à la fois **égales en valeur** et **du même type**.

    ```php
    $a = 5;           // entier
    $b = "5";         // chaîne de caractères

    var_dump($a == $b);   // true (même valeur)
    var_dump($a === $b);  // false (pas le même type)
    ```

   **Explication :** Avec l’opérateur `===`, la comparaison entre `$a` et `$b` renvoie `false` car bien qu'ils aient la même valeur (`5`), ils n'ont pas le même type (`$a` est un entier et `$b` est une chaîne).

4. **Différent strict (`!==`)** : Vérifie si deux valeurs sont **différentes en valeur** ou **de type différent**.

    ```php
    $x = 10;          // entier
    $y = "10";        // chaîne de caractères

    var_dump($x != $y);    // false (même valeur)
    var_dump($x !== $y);   // true (pas le même type)
    ```

   **Explication :** L'opérateur `!==` retourne `true` si les deux variables sont soit de type différent, soit de valeur différente, ou les deux. Ici, même si `$x` et `$y` ont la même valeur (`10`), ils sont de types différents (`int` et `string`), donc la comparaison `!==` retourne `true`.

5. **Supérieur à (`>`)** et **Inférieur à (`<`)** : Compare si une valeur est plus grande ou plus petite qu'une autre.
   ```php
   var_dump(5 > 3); // true
   var_dump(3 < 5); // true
   ```

6. **Supérieur ou égal (`>=`)** et **Inférieur ou égal (`<=`)** : Compare si une valeur est plus grande ou égale, ou plus petite ou égale qu'une autre.
   ```php
   var_dump(5 >= 5); // true
   var_dump(3 <= 4); // true
   ```

#### Opérateurs Logiques

Les opérateurs logiques permettent de combiner des conditions.

1. **ET logique (`&&` / `and`)** : Renvoie `true` si toutes les conditions sont vraies.
   ```php
   $a = 5;
   var_dump($a > 3 && $a < 10); // true
   ```

2. **OU logique (`||` / `or`)** : Renvoie `true` si au moins une des conditions est vraie.
   ```php
   $a = 5;
   var_dump($a < 3 || $a < 10); // true
   ```

3. **NON logique (`!`)** : Renverse la valeur d'une condition.
   ```php
   $a = true;
   var_dump(!$a); // false
   ```

---

### Example de manipulation de variables:

```php
$a = 5;
$b = 10;
$addition = $a + $b;
echo "La somme est : " . $addition;
```

---

## 3. Structures de Contrôle en PHP

### Conditions

- **Structure `if`, `else`, `elseif`** :

    ```php
    $age = 18;
    if ($age >= 18) {
        echo "Vous êtes majeur.";
    } else {
        echo "Vous êtes mineur.";
    }
    ```

- **Structure `switch`** pour évaluer plusieurs valeurs possibles.

   ```php
   $jour = "samedi";

   switch ($jour) {
       case "lundi":
       case "mardi":
       case "mercredi":
       case "jeudi":
       case "vendredi":
           echo "C'est un jour de semaine";
           break;
       case "samedi":
       case "dimanche":
           echo "C'est le week-end";
           break;
       default:
           echo "Jour inconnu";
           break;
   }
   ```
- **La structure `match`** est similaire à switch mais plus concise et retourne directement une valeur.
   ```php
   $jour = "samedi";
   
   $message = match ($jour) {
       "lundi", "mardi", "mercredi", "jeudi", "vendredi" => "C'est un jour de semaine",
       "samedi", "dimanche" => "C'est le week-end",
       default => "Jour inconnu",
   };
   
   echo $message;
   ```

### Boucles

- **Boucle `for`** : pour un nombre connu d'itérations.

    ```php
    for ($i = 1; $i <= 10; $i++) {
        echo $i;
    }
    ```

- **Boucle `while`** : pour une condition vraie.

    ```php
    $i = 1;
    while ($i <= 10) {
        echo $i;
        $i++;
    }
    ```

- **Boucle `foreach`** : pour parcourir un tableau.

    ```php
    $couleurs = ["rouge", "bleu", "vert"];
    foreach ($couleurs as $couleur) {
        echo $couleur;
    }
    ```


---

## 4. Fonctions en PHP

- Une **fonction** est un bloc de code qui effectue une tâche précise.
- Elle peut être réutilisée à plusieurs endroits dans un programme, ce qui permet de réduire la duplication du code.
- Les fonctions permettent de rendre le code plus structuré et plus facile à lire.
- Il existe un grand nombre de fonctions fournies par le langage PHP que l'on peut utiliser dans notre code

### Utilisation d'une fonction

On peut utiliser une fonction de la manière suivante:

```php

```

### Structure d'une fonction

La syntaxe de base d’une fonction en PHP est la suivante :

```php
function nomDeLaFonction($parametre1, $parametre2) {
    // Bloc de code à exécuter
    return $resultat; // optionnel
}
```

- **nomDeLaFonction** : Le nom de la fonction. Il doit être significatif pour refléter ce que fait la fonction.
- **Paramètres** : Valeurs fournies à la fonction pour qu’elle puisse exécuter son code. Ils sont optionnels.
- **Return** : Utilisé pour renvoyer un résultat de la fonction.

### Exemple de fonction simple

Créons une fonction qui affiche un message de bienvenue.

```php
function afficherBienvenue() {
    echo "Bienvenue dans notre programme !";
}

afficherBienvenue(); // Appel de la fonction
```

### Fonction avec Paramètres

Une fonction peut recevoir des **paramètres** pour être plus flexible. Par exemple, nous allons créer une fonction qui salue un utilisateur en fonction de son nom :

```php
function direBonjour($nom) {
    return "Bonjour, " . $nom . " !";
}

echo direBonjour("Alice"); // Affiche "Bonjour, Alice !"
```

Dans cet exemple, `$nom` est un paramètre passé à la fonction `direBonjour`. La fonction l’utilise pour personnaliser le message de retour.

### Fonction avec Plusieurs Paramètres

Les fonctions peuvent aussi recevoir plusieurs paramètres pour effectuer des opérations plus complexes. Voici une fonction qui additionne deux nombres :

```php
function addition($a, $b) {
    return $a + $b;
}

echo addition(5, 10); // Affiche 15
```

### Valeur de Retour

La **valeur de retour** est le résultat qu’une fonction envoie au programme. Elle est utilisée avec le mot-clé `return`. Lorsqu'une fonction atteint une instruction `return`, elle arrête immédiatement son exécution et renvoie le résultat indiqué.

**Exemple :**

```php
function calculerSurface($longueur, $largeur) {
    $surface = $longueur * $largeur;
    return $surface;
}

echo calculerSurface(5, 10); // Affiche 50
$surface = calculerSurface(5, 10); // Affiche 50
```

Dans cet exemple, la fonction `calculerSurface` renvoie la surface d’un rectangle calculée à partir des paramètres `$longueur` et `$largeur`.

---

### Portée des Variables dans une Fonction

La portée des variables fait référence à l'endroit où elles sont accessibles dans le code. Les variables définies **à l’intérieur d’une fonction** sont locales à cette fonction et ne peuvent pas être utilisées en dehors de celle-ci.

```php
function exemple() {
    $message = "Hello";
    echo $message;
}

exemple(); // Affiche "Hello"
echo $message; // Erreur : $message n'existe pas en dehors de la fonction
```

---





## 5. Les Tableaux en PHP

- Un **tableau** est une structure de données qui permet de stocker plusieurs valeurs dans une seule variable.
- Les tableaux sont utilisés pour organiser et manipuler des ensembles de données.

---

### Tableaux Indexés

Un **tableau indexé** est un tableau où chaque élément est associé à un indice numérique.

#### Déclaration d'un tableau indexé

```php
$couleurs = ["rouge", "vert", "bleu"];
```

#### Accès aux éléments d'un tableau

```php
echo $couleurs[0]; // Affiche "rouge"
echo $couleurs[1]; // Affiche "vert"
```

#### Ajouter des éléments à un tableau

```php
$couleurs[] = "jaune";
print_r($couleurs); // Affiche ["rouge", "vert", "bleu", "jaune"]
```

---

### Fonctions Utiles pour Manipuler les Tableaux

PHP propose plusieurs fonctions pratiques pour manipuler les tableaux.

- **`count($tableau)`** : Retourne le nombre d'éléments dans le tableau.

    ```php
    echo count($couleurs); // Affiche 4 si $couleurs = ["rouge", "vert", "bleu", "jaune"]
    ```

- **`array_push($tableau, $element)`** : Ajoute un élément à la fin du tableau.

    ```php
    array_push($couleurs, "violet");
    print_r($couleurs); // ["rouge", "vert", "bleu", "jaune", "violet"]
    ```

- **`array_pop($tableau)`** : Retire le dernier élément du tableau.

    ```php
    array_pop($couleurs);
    print_r($couleurs); // ["rouge", "vert", "bleu", "jaune"]
    ```

- **`array_keys($tableau)`** : Retourne toutes les clés d'un tableau associatif.

    ```php
    print_r(array_keys($personne)); // Affiche ["nom", "age", "ville", "pays"]
    ```

- **`array_values($tableau)`** : Retourne toutes les valeurs d'un tableau associatif.

    ```php
    print_r(array_values($personne)); // Affiche ["Dupont", 31, "Paris", "France"]
    ```

---

### Parcourir un Tableau avec des Boucles

Les boucles `for` et `foreach` sont souvent utilisées pour parcourir les éléments d'un tableau.

#### Utilisation de `for`

```php
for ($i = 0; $i < count($couleurs); $i++) {
    echo $couleurs[$i] . "\n";
}
```

#### Utilisation de `foreach` avec un tableau

```php
foreach ($personne as $cle => $valeur) {
    echo "$cle : $valeur\n";
}
```

---

### Tableaux Associatifs

Un **tableau associatif** est un tableau où chaque élément est associé à une clé unique. Ce type de tableau est très utile pour organiser les données sous forme de paires clé/valeur.

#### Déclaration d'un tableau associatif

```php
$personne = [
    "nom" => "Dupont",
    "age" => 30,
    "ville" => "Paris"
];
```

#### Accès aux éléments d'un tableau associatif

```php
echo $personne["nom"];  // Affiche "Dupont"
echo $personne["ville"]; // Affiche "Paris"
```

#### Ajouter ou modifier des éléments

```php
$personne["pays"] = "France";
$personne["age"] = 31;
print_r($personne);
```

---

### Tableaux Multidimensionnels

Un **tableau multidimensionnel** est un tableau qui contient un ou plusieurs tableaux en tant qu'éléments.

#### Exemple de tableau multidimensionnel

```php
$etudiants = [
    ["nom" => "Alice", "age" => 20, "ville" => "Lyon"],
    ["nom" => "Bob", "age" => 22, "ville" => "Marseille"],
    ["nom" => "Charlie", "age" => 23, "ville" => "Paris"]
];
```

#### Accéder aux éléments d'un tableau multidimensionnel

```php
echo $etudiants[0]["nom"]; // Affiche "Alice"
echo $etudiants[1]["ville"]; // Affiche "Marseille"
```


