---
title: Tuto Gitlab CI/CD en SSH
---

## 1/ script de déployement 
Créer un script de déployement bash (exemple projet : tualeuchoa) 
```shell
#! bin/bash


git pull

composer install

yarn install

yarn build

doctrine:migration:migrate
```
Dans cette exemple j'ai mis dans le script bash les commandes nécessaires pour que le deploiement ce passe bien: 
* j'actualise mon repot 
* je reinstalle composer au cas ou il y'a de nouvelles dépendances
* yarn pour compiler mon scss + si y'a de nouvelles dépandances
* migration avec doctrine au cas ou il y'a du changement dans la bdd

**il faut le mettre le deploy.sh dans la racine du projet.**


## 2/ .gitlab-ci.yaml 
Créer un fichier .gitlab-ci.yaml à la racine du projet 
### contenu du gitlab-ci.yaml

```yaml
image: tetraweb/php

before_script:
  - 'which ssh-agent || ( apt-get install -qq openssh-client )'
  - eval $(ssh-agent -s)
  - ssh-add <(echo "$SSH_PRIVATE_KEY")
  - mkdir -p ~/.ssh
  - '[[ -f /.dockerenv ]] && echo -e "Host *\n\tStrictHostKeyChecking no\n\n" > ~/.ssh/config'


deploy_develop:

  script:
    - ssh $SSH_USER@$SSH_HOST "cd $PROJECT_FOLDER && sh deploy.sh"
  only:
    - dev
```

`image` = l'image docker sur la quel on se base ( on a seulement  besoin de se connecter en ssh sur le serveur ou la il y'a tout ce qu'on a besoin sur le projet à ne pas confondre avec le serveur justement)

`before script` = à copier coller par default

`script` = pour que gitlab execute notre script bash du debut il doit se connecter au serveur grace au variable d'environement : 

* `$SSH_USER`: "codecolliders" 
* `$SSH_HOST`: c'est l'ip du serveur pour se connecter en ssh
* `$PROJET_FOLDER`: le chemin absolut du projet

Pour definir les variables d'environnement dans GitLab directement il faut aller dans setting/CI/CD et chercher variable. À la fin de la commande on vient lancer le script bash de la première etapes

```yaml
 && sh deploy.sh
```

## 3/ Résultat
Dans l'onglet GitLab CI/CD il devrait vous affichez un test 'passed' au prochaine commit sur la branche 'dev'
![](/home/riku/Images/pipeline200.png)
