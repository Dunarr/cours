---
title: Conception de base de données
---

## dictionnaire des données

C'est une étape intermédiaire qui peut avoir son importance, surtout si vous êtes plusieurs à travailler sur une même base de données, d'un volume important.
Le dictionnaire des données est un document qui regroupe toutes les données que vous aurez à conserver dans votre base. Pour chaque donnée, il indique :

**la désignation :** il s'agit d'une mention décrivant ce à quoi la donnée correspond (par exemple «titre du livre») ;

**le type de donnée :**
- Numérique : lorsque la donnée est composée uniquement de nombres (entiers ou réels),
- Alphanumérique : un texte
- Date : lorsque la donnée est une date (au format AAAA-MM-JJ),
- Booléen : Vrai ou Faux

**la taille :** elle s'exprime en nombre de caractères ou de chiffres la taille approximative de la donnée à stocker

### Remarques:

- Les données qui figurent dans le MCD (et donc dans le dictionnaire des données) doivent être, dans la plupart des cas, élémentaires :
  - elles ne doivent pas être calculées : les données calculées doivent être obtenues, par le calcul, à partir de données élémentaires qui, elles, sont conservées en base.
  - elles ne doivent pas être composées : les données composées doivent être obtenues par la concaténation de données élémentaires conservées en base. Par exemple une adresse est obtenue à partir d'une rue, d'une ville et d'un code postal : ce sont ces trois dernières données qui sont conservées et donc qui figureront dans le MCD (et dans le dictionnaire des données).
- Lorsque l'on n'effectue jamais de calcul sur une donnée numérique, celle-ci doit être de type AN (c'est le cas par exemple pour un numéro de téléphone).
- Dans la plus part des cas, les données ne doivent pas être dupliquées, on ne doit pas retrouver la même information dans plusieures tables du MCD
- Dans la majorité des cas, un attribut d'une entité ne doit contenir que des valeurs uniques (pour chaque entrée, une case ne peut contenir qu'une seule information)


### Example:
Pour un blog: 
 - titre article: Alphanumérique
 - slug article: Alphanumérique
 - Nombre de vues: Nombre  
 - article publié: Booléen 
 - contenu article: Alphanumérique
 - date de publication: Date
 - pseudo auteur: Alphanumérique
 - photo de profile: Alphanumérique
 - nom catégorie: Alphanumérique
 - slug catégorie: Alphanumérique
 - nom tag: Alphanumérique
 - slug tag: Alphanumérique

## MCD
Chaque entité est unique et est décrite par un ensemble de propriétés encore appelées attributs ou caractéristiques. 
On regroupe dans l'entité, l'ensemble des propriétés qui permettent de décrire chaque instance.

Une des propriétés de l'entité est l'identifiant. Cette propriété doit posséder des occurrences uniques et doit être source des dépendances fonctionnelles avec toutes les autres propriétés de l'entité.
Bien souvent, on utilise une donnée de type entier qui s'incrémente pour chaque occurrence, ou encore un code unique spécifique du contexte.

Les entités sont replésentées par des tableaux dont le titre est le nom de l'entité et le contenu liste les attributs   

| article      | category | tag    | author          |
|--------------|----------|--------|-----------------|
| **id**       | **id**   | **id** | **id**          |
| title        | title    | title  | username        |
| slug         | slug     | slug   | profile_picture |
| content      |          |        |                 |
| published_at |          |        |                 |
| views        |          |        |                 |
| published    |          |        |                 |





### Les associations

Une association définit un lien sémantique entre une ou plusieurs entités. Elle permet de créer un lien logique qui pourra être exploité dans l'application.

Cette liaison est symbolisée par un trait reliant les entités, un verbe definissant la relation et des cardinalités. 

Les cardinalités sont des indications indiquant le nombre d'instances qui peuvent être liées, il en existe 4 types de cardinalités principales:
- **0,1**: une instance de l'entité A peut être reliée à minimum 0 et maximum 1 instances de l'entité B
- **1,1**: une instance de l'entité A peut être reliée à minimum 1 et maximum 1 instances de l'entité B
- **0,n**: une instance de l'entité A peut être reliée à minimum 0 et maximum ∞ instances de l'entité B
- **1,n**: une instance de l'entité A peut être reliée à minimum 1 et maximum ∞ instances de l'entité B


![](@@/conception-mcd.png)


## Le passage du MCD au MLD et SQL
Le MCD peut être traduit en MLD dont la structure sera directement utilisable pour créer la base de donéees 
Les entités deviennent alors des tables et les attributs deviennent des colonnes.
Les associations se transforment en relations.

### Les relations
Les associations du MCD se traduisent en relation dans le MLD. 
Les relations se créent grâce à des attributs spécifiques, des clefs étrangéres (foreign key) qui référencent un identifiant,
(pour lier les entités A et B, on apeut ajouter un attribut b_id dans l'entité A, on pourra ainsi stocker l'identifiant de l'instance de B que l'on souhaite lier)
![](@@/conception-mld-1.png)


### Conversion des associations en relations
Pour convertir les associations en relations, on utilise les maximums cardinalités
(par example, pour la relation entre category et article, les cardinalités 1,1-0,n devient une relation 1-n)
En fonction du type de relation, celà se traduira différent dans le MLD.

#### Relation one to many (1-n) ou many to one (n-1)
Dans le cas d'une relation one to many ou many to one, on ajoutera une clef étrangère coté many (n)

#### Relation one to one (1-1) 
Il s'agit d'un cas qui n'arrive que rarement
Dans le cas d'une relation one to one on ajoutera une clef étrangère du coté de l'entité que l'on considère comme principale

#### Relation many to many (n-n) 
Dans le cas d'une relation many to many, on ne peut pas se contenter d'ajouter un attribut dans l'une des deux entités.
Il faut ajouter une nouvelle table (une table pivot) qui contiendra deux clef étrangères, une pour chaque entité.

![conception-mld-2.png](@@/conception-mld-2.png)

### Typage
Dans la plus part des cas, on ajoutera les types des attributs dans le MLD en fonction des types disponnibles dans le SGBD ciblé

