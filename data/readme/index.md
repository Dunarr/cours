---
title: README
---

## structure

### project structure

```
assets
  image.png
  asciinema.cast
data => pages
  index.json
  page1
    index.md
  page2
  meta.js
    index.html
  page3
    meta.json
    index.md
dist => output
themes => custom themes
  main
    README.md => theme documentation for user
    config.js => theme declaration
    partiels
      menu.mustache
      teaser.mustache
    layouts
      default.mustache
      home.mustache
    assets
      style.css
      script.js
      icon.png
src => temporary core folder
package.json
config.js
yarn.lock
.env
.env.dist
.gitignore
```

### Aliases


 - `\@/  => project root directory => @/`
 - `\@@/ => assets directory => @@/`


### architecture

#### build

load data


## settings

### /config.js

```js
const theme = require("./themes/main/config.js")
const path = require("path")

module.exports = {
    theme: theme, // project's theme
    dataDir: path.join(__dirname,"data"), // project's data directory
    assetsDir: path.join(__dirname,"assets"), // project's assets directory
    outDir: path.join(__dirname,"dist"), // project's output directory
    publicUrl: process.env.PUBLIC_URL || "", // project's web url (change it if the project is in sub-folder on your server)
    hljs_langs: [ // highlight.js loaded languages (see: https://github.com/highlightjs/highlight.js/tree/main/src/languages)
        "apache",
        "bash",
        "css",
        "javascript",
        "json",
        "markdown",
        "nginx",
        "php",
        "scss",
        "shell",
        "sql",
        "twig",
        "typescript",
        "xml",
        "yaml",
    ],
    globals: {} // global variables for templates
}
```

### .env

```dotenv
PUBLIC_URL="" #project's public url (see /config.js)
```

### THEME/config.js

```js
module.exports = {
    "root": __dirname,
    "readme_template": "default",
    "init": app => {}  // optional,
}
```

## theme building

### predefined variables

 - chapters => page title structure
 - title => page title
 - styles 
 - scripts

### components
