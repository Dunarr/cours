# Rappel algorithmique + JS

## Variables

### Variables et constantes
Dans un algorithme, on peut conserver des données dans une variable ou une constante.  
Une variable est comme une boîte qui contient une valeur que l'on peut modifier ou lire au cours du programme.  
Une constante est comme une variable, mais ne peut pas être modifiée après avoir été créée.

```javascript
// Déclaration d'une variable
let maVariable;
// Déclaration d'une variable avec une valeur
let maVariable2 = 3;
// Déclaration d'une constante
const maConstante = 5;
```

## Opérateurs

### Opérateurs d'affectation
```javascript
let variable1 = "Bonjour";
variable1 = "Salut";

let nombre = 10;
nombre += 5; // nombre = nombre + 5
nombre -= 5; // nombre = nombre - 5
nombre *= 5; // nombre = nombre * 5
nombre /= 5; // nombre = nombre / 5
nombre **= 5; // nombre = nombre ** 5
nombre %= 5; // nombre = nombre % 5

nombre++;
nombre--;
```

### Opérateurs arithmétiques
```javascript
console.log(3 + 2);
console.log(3 - 2);
console.log(3 * 2);
console.log(3 / 2);

console.log(3 ** 2); // puissance
console.log(10 % 3); // modulo (reste de la division)
```

### Opérateur de concaténation
```javascript
console.log("Bon" + "jour");
```

### Opérateurs de comparaison
```javascript
console.log(nombre === 10);
console.log(nombre !== 10);
console.log(nombre > 10);
console.log(nombre >= 10);
console.log(nombre < 10);
console.log(nombre <= 10);
console.log(nombre == 10);
console.log(nombre != 10);
```

### Opérateurs booléens
```javascript
console.log(nombre < 10 || nombre > 10);
console.log(nombre === 10 && texte === "bonjour");

let age = 20;
console.log(age < 25); // => true
console.log(age > 25); // => false

console.log(age > 18 && age < 25);
console.log(age < 25 || age > 100);
```

### Les valeurs
Dans un programme, il y a des valeurs, elles peuvent être écrites en dur, retournées par des fonctions, stockées dans des variables, etc.
```javascript
5 // Ici, 5 est un nombre
let number = 5; // La variable number contient la valeur 5
console.log(number); // On peut lire le contenu de la variable pour le passer à une fonction (ici pour l'afficher dans la console)
let number2 = number; // On peut aussi lire le contenu d'une variable pour la copier

const response = prompt("Quel âge avez-vous ?"); // Ici, la fonction prompt retourne une valeur que l'on stocke dans la constante response
```

### Types de valeurs
Il existe différents types de valeurs correspondant à chaque type de données que l'on peut manipuler :
```javascript
let nombre = 10; // number => nombre (entier/à virgule/positif/négatif)
let vrai = true; // boolean => vrai ou faux
let faux = false; // boolean => vrai ou faux
let texte = "Hello World"; // string => texte
let rien = null; // null => rien
let vide = undefined; // undefined => vide
let variable; // Variable vide = undefined
```

## Conditions
Les conditions permettent d'exécuter du code si une certaine condition est vraie.  
Exemple :
```js
if (age >= 18) {
    console.log("Vous êtes majeur"); 
}
```

La condition est indiquée dans les parenthèses qui suivent le mot-clé `if`.  
Elle peut prendre plusieurs formes, mais doit correspondre à un booléen :
- `if (age > 10) {...}`
- `if (true) {...}`
- `if (estMajeur) {...}` (ici, `estMajeur` est une variable créée précédemment qui contient `true` ou `false`).

Après un `if`, on peut ajouter un `else`. Cela permet d'indiquer le code à exécuter si la condition dans le `if` n'est pas remplie.  
Exemple :
```js
if (age >= 18) {
    console.log("Vous êtes majeur"); 
} else {
    console.log("Vous êtes mineur");
}
```

Enfin, il existe aussi le `else if(...)` qui permet, comme le `else`, d'indiquer le code à exécuter si une condition est fausse, mais seulement si une seconde condition est vraie :
```js
if (age < 18) {
    // Ce code ne sera exécuté que si :
    // La condition "age < 18" est vraie
    console.log("Vous êtes mineur"); 
} else if (age < 150) {
    // Ce code ne sera exécuté que si :
    // La condition "age < 18" est fausse
    // Et la condition "age < 150" est vraie
    console.log("Vous êtes majeur"); 
} else {
    // Ce code ne sera exécuté que si :
    // La condition "age < 18" est fausse
    // Et la condition "age < 150" est fausse aussi
    console.log("Vous n'êtes probablement plus de ce monde");
}
```

## Boucles
Les boucles permettent en algorithmique de répéter une action plusieurs fois.  
Il en existe trois types principaux.

### while (et do...while)
La boucle `while` (tant que "condition") est la boucle la plus générique.  
Elle répète une action tant qu'une condition donnée est vraie.  
Exemple :
```js
let stockDeChaises = 10;
while (stockDeChaises > 0) {  // Tant que j'ai du stock de chaises, je vends des chaises
    console.log("Je vends une chaise");
    stockDeChaises--;
}
```
La boucle `while` permet de répéter une action avec beaucoup de conditions différentes.

### for
La boucle `for` (pour "i" allant de 0 à "n") permet de répéter une action un nombre de fois connu à l'avance.  
Le nombre de fois que la boucle doit être répétée peut soit être écrit dans le programme (ex : j'affiche 15x "bonjour")  
Il peut aussi venir d'une variable (ex : je demande un nombre à l'utilisateur et je répète l'action autant de fois que demandé).

Exemples :
```js
// On affiche 15x "bonjour"
for (let i = 0; i < 15; i++) {
  console.log("Bonjour");
}

const num = parseInt(prompt("Saisissez un nombre"));
for (let i = 0; i < num; i++) {
  console.log("Bonjour");
}
```

La boucle `for` peut aussi servir à parcourir un tableau.  
En effet, pour effectuer une action sur les éléments d'un tableau, il faut récupérer chaque élément un par un.  
Il faut donc répéter notre action autant de fois que l'on a d'éléments dans le tableau.  
Pour cela, la boucle `for` paraît plutôt indiquée.  
Exemple :
```js
const legumes = ["tomate", "concombre", "citrouille", "choux"];
for (let i = 0; i < legumes.length; i++) {
  // À chaque tour, i prendra un nombre entre 0 et la taille du tableau (0, 1, 2, 3)
  // Ainsi, au premier tour, on récupère l'élément en position 0
  // Puis en position 1, puis 2, puis 3
  console.log(legumes[i]);
}
```

### for of
Pour traiter (parcourir) chaque élément d'un tableau, il existe une boucle encore plus simple, le `forEach`.  
Elle peut s'écrire de deux façons différentes : `.forEach` ou `for of`.  
Les deux codes suivants sont équivalents :
```js
const legumes = ["tomate", "concombre", "citrouille", "choux"];
legumes.forEach(function (legume) {
  // La boucle forEach insère automatiquement les valeurs du tableau dans la variable legume
  // Je récupère donc "tomate", puis "concombre", etc.
  console.log(legume);
});
```
```js
const legumes = ["tomate", "concombre", "citrouille", "choux"];
for (const legume of legumes) {
  // La boucle for of insère automatiquement les valeurs du tableau dans la variable legume
  // Je récupère donc "tomate", puis "concombre", etc.
  console.log(legume);
}
```

## Fonctions
Une fonction est un morceau de code que je peux réutiliser et exécuter plus tard, en JavaScript.  
Elle permet aussi, entre autres, de déclencher du code après une action de l'utilisateur.  
Par contre, si une fonction n'est pas exécutée, le code à l'intérieur ne sera pas exécuté non plus.  
Je peux écrire tout le code que je veux dans une fonction.

```javascript
function maFonction() {
    console.log("TEST!");
}
// Pour le moment, rien n'a été exécuté.

maFonction(); // Affiche "TEST!"
maFonction(); // Affiche "TEST!"
maFonction(); // Affiche "TEST!"
```

### Paramètres
Si une fonction utilise des valeurs qui sont créées en dehors, je peux utiliser des paramètres.  
Cela me permet, par exemple, de changer le comportement d'une fonction au moment où je l'exécute ou bien de lui fournir des informations que j'ai prises ailleurs.
```javascript
function direBonjour(prenom) {
    console.log("Bonjour " + prenom);
}

direBonjour("John"); // Affiche "Bonjour John"
direBonjour("Marc"); // Affiche "Bonjour Marc"
direBonjour("Paul"); // Affiche "Bonjour Paul"
```
Ici, `prenom` est un **paramètre**, il agit comme une variable mais son contenu n'est affecté que lorsque j'appelle la fonction.  
Par exemple, lorsque j'écris `direBonjour("John")` (ici, "John" est un **argument**), le code est exécuté et `prenom` est égal à "John".

Je peux aussi créer une fonction avec plusieurs paramètres (que j'appellerai donc avec plusieurs arguments) en mettant une virgule entre chaque paramètre et argument.

```javascript
function additionner(nombreA, nombreB) {
    console.log("Résultat: " + (nombreA + nombreB));
}

additionner(1, 2); // Affiche "Résultat: 3"
additionner(5, 6); // Affiche "Résultat: 11"
```

### Valeur de retour
Dans les fonctions précédentes, nous faisions des `console.log` pour afficher un résultat.  
Cela permet à un humain (si il sait utiliser l'inspecteur du navigateur) de voir un résultat, mais pas au programme de le récupérer.  
Dans certains cas, on peut avoir besoin d'utiliser le résultat d'une fonction plus tard dans le code.  
Pour cela, la fonction peut retourner un résultat en utilisant le mot-clé `return`.

```javascript
function additionner(nombreA, nombreB) {
    console.log("Résultat: " + (nombreA + nombreB));
}

additionner(1, 2); // Affiche "Résultat: 3", mais je ne peux pas réutiliser le résultat plus tard

function additionner_2(nombreA, nombreB) {
  return "Résultat: " + (nombreA + nombreB);
}

const resultat = additionner_2(1, 2); // La variable resultat contient "Résultat: 3", mais rien n'est affiché.

console.log(resultat); // Affiche "Résultat: 3"
```


## Tableaux
Un tableau est une structure de données qui permet de stocker plusieurs valeurs dans une seule variable.

```javascript
// Création d'un tableau
let fruits = ["pomme", "banane", "orange"];

// Accès aux éléments (l'index commence à 0)
console.log(fruits[0]); // affiche "pomme"
console.log(fruits[1]); // affiche "banane"

// Modification d'un élément
fruits[2] = "citron";

// Propriété length pour connaître la taille du tableau
console.log(fruits.length); // affiche 3
```

### Méthodes principales des tableaux
```javascript
let monTableau = ["a", "b", "c"];

// Ajouter à la fin
monTableau.push("d"); // ["a", "b", "c", "d"]

// Retirer le dernier élément
let dernier = monTableau.pop(); // ["a", "b", "c"]

// Ajouter au début
monTableau.unshift("z"); // ["z", "a", "b", "c"]

// Retirer au début
let premier = monTableau.shift(); // ["a", "b", "c"]

// Découper une partie du tableau
let portion = monTableau.slice(1, 3); // ["b", "c"]

// Modifier le tableau (supprimer/ajouter des éléments)
monTableau.splice(1, 1, "x", "y"); // remplace 1 élément à partir de l'index 1
monTableau.splice(2, 1); // supprime un élément à l'index 1
```

### Méthodes de parcours
```javascript
const nombres = [1, 2, 3, 4, 5];

// map : transformer chaque élément
const doubles = nombres.map(nombre => nombre * 2);
// résultat : [2, 4, 6, 8, 10]

// filter : filtrer les éléments
const pairs = nombres.filter(nombre => nombre % 2 === 0);
// résultat : [2, 4]

// reduce : réduire le tableau à une seule valeur
const somme = nombres.reduce((acc, nombre) => acc + nombre, 0);
// résultat : 15
```

## Objets
Un objet est une collection de paires clé-valeur qui permet de regrouper des données liées entre elles.

```javascript
// Création d'un objet
const personne = {
    nom: "Dupont",
    prenom: "Jean",
    age: 30,
    adresse: {
        rue: "123 rue principale",
        ville: "Paris"
    }
};

// Accès aux propriétés
console.log(personne["prenom"]); // avec la notation crochets
console.log(personne.nom); // avec la notation point

// Modification d'une propriété
personne.age = 31;

// Ajout d'une nouvelle propriété
personne.email = "jean.dupont@email.com";
```

### Méthodes d'objets
```javascript
const voiture = {
    marque: "Peugeot",
    modele: "308",
    demarrer: function() {
        console.log("La voiture démarre");
    },
    // Syntaxe raccourcie pour les méthodes
    klaxonner() {
        console.log("Beep beep!");
    },
    displayDescription() {
        // la variable "this" permet d'accéder à l'objet contenant la méthode 
        console.log(`This is a ${this.marque} ${this.modele}`)
    }
};

voiture.demarrer(); // appel de la méthode
```

### Manipulation d'objets
```javascript
const utilisateur = {
    nom: "Martin",
    age: 25
};

// Récupérer toutes les clés
const cles = Object.keys(utilisateur);
// résultat : ["nom", "age"]

// Récupérer toutes les valeurs
const valeurs = Object.values(utilisateur);
// résultat : ["Martin", 25]

// Récupérer les paires clé-valeur
const entrees = Object.entries(utilisateur);
// résultat : [["nom", "Martin"], ["age", 25]]

// Vérifier si une propriété existe
console.log("nom" in utilisateur); // true
console.log(utilisateur.hasOwnProperty("nom")); // true
```

### Déstructuration d'objets
```javascript
const produit = {
    nom: "Ordinateur",
    prix: 999,
    stock: 5
};

// Extraction de propriétés dans des variables
const { nom, prix } = produit;
console.log(nom); // "Ordinateur"
console.log(prix); // 999

// Avec renommage
const { nom: nomProduit } = produit;
console.log(nomProduit); // "Ordinateur"
```

## Fonctions avancées

### Fonctions récursives
Une fonction récursive est une fonction qui s'appelle elle-même. Elle permet de résoudre des problèmes qui peuvent être décomposés en sous-problèmes similaires mais plus simples.
Celà fonctionne comme une boucle qui va répéter du code.

Voici un exemple simple avec le calcul de la factorielle :
```javascript
function factorielle(n) {
    // Cas de base : si n est 0 ou 1, on retourne 1
    if (n <= 1) {
        return 1;
    }
    // Cas récursif : on multiplie n par la factorielle de (n-1)
    return n * factorielle(n - 1);
}

console.log(factorielle(5)); // affiche 120 (5 * 4 * 3 * 2 * 1)
```

Points importants pour une fonction récursive :
1. Elle doit avoir une condition d'arrêt (cas de base)
2. Elle doit se rapprocher du cas de base à chaque appel
3. Elle doit s'appeler elle-même avec un problème plus simple

Autre exemple avec le calcul de la suite de Fibonacci :
```javascript
function fibonacci(n) {
    // Cas de base : les deux premiers nombres sont 0 et 1
    if (n <= 0) return 0;
    if (n === 1) return 1;
    
    // Cas récursif : on additionne les deux nombres précédents
    return fibonacci(n - 1) + fibonacci(n - 2);
}

console.log(fibonacci(6)); // affiche 8 (car 0, 1, 1, 2, 3, 5, 8)
```

Les fonctions récursives sont particulièrement utiles pour :
- Parcourir des structures de données arborescentes
- Résoudre des problèmes mathématiques
- Implémenter des algorithmes de tri (comme le tri fusion)
- Parcourir des systèmes de fichiers


### Callback functions
Une fonction peut être considérée comme n'importe quel type de données.  
Je peux donc la stocker ou la copier dans une variable :
```js
function maFonction() {
    console.log("TEST!");
}

// Va afficher la "référence" de la fonction, le code à l'intérieur ne sera pas exécuté
console.log(maFonction);  

// On peut copier une fonction dans une variable et utiliser celle-ci comme la fonction de base
const cloneMaFonction = maFonction;
maFonction(); // TEST!
cloneMaFonction(); // TEST!

// Je peux donc aussi directement créer une fonction et la stocker dans une variable
const maNouvelleFonction = function() {
    console.log("TEST2!");
};
maNouvelleFonction(); // TEST2!
```

En allant plus loin, on peut dire que la majorité des choses faisables avec une variable le sont avec une fonction.

Petit rappel des paramètres :
```js
function afficher(donnee) {
    console.log(donnee);
}
afficher("Hello"); // Affiche "Hello"
const prenom = "William";
afficher(prenom); // Affiche "William"
afficher(maFonction); // Affiche la référence à "maFonction"
```

Puisque je peux donner une fonction "A" en paramètre à une fonction "B", je peux donc exécuter la fonction reçue en paramètre dans la fonction "B".  
Exemple :
```js
function direBonjour(prenom) {
    console.log("Bonjour " + prenom);
}

function interagirAvecUtilisateur(uneFonction) {
    const prenomUtilisateur = "William";
    uneFonction(prenomUtilisateur);
}

interagirAvecUtilisateur(direBonjour); // Affiche "Bonjour William"
```

Ici, je passe la fonction `direBonjour` en argument au paramètre `uneFonction`, donc lorsque le code s'exécute, `uneFonction` contiendra le code (la référence) de `direBonjour`.  
Je peux donc l'exécuter de la même manière que n'importe quelle fonction, avec des parenthèses.  
Lorsque l'on écrit `uneFonction(prenomUtilisateur)`, on exécute donc la fonction `direBonjour`.

L'intérêt principal de créer un code avec des "callback functions" est de le rendre plus générique et réutilisable, ex :
```js
function direBonjour(prenom) {
    console.log("Bonjour " + prenom);
}

function direAurevoir(prenom) {
    console.log("Au revoir " + prenom);
}

function interagirAvecUtilisateur(uneFonction) {
    const prenomUtilisateur = prompt('Comment vous appelez-vous ?');
    uneFonction(prenomUtilisateur);
}
```

La principale utilisation des "callback functions" en JS est pour utiliser des fonctions fournies par le langage telles que :
- `sort`
- `filter`
- `reduce`
- `find`
- `addEventListener`

Qui prennent toutes des fonctions en paramètre.  
Exemple :
```js
function fonctionDeTri(numberA, numberB) {
  return numberA - numberB;
}

const monTableau = [1, 15, 7, 100, 2, 18, 5];

monTableau.forEach(function (el) {
  console.log(el);
});

monTableau.sort(fonctionDeTri);
console.log(monTableau);
```
