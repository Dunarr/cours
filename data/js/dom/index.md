# Javascript DOM
La manipulation de l'HTML en javascript passe par le **DOM**. Il s'agit de récupérer les balises avec javascript pour les manipuler sousforme d'objet 

## Récupérer une balise html
La première étape pour modifier une balise HTML est de la récupérer dans le Javascript.
Pour ça il existe plusieures méthodes:
 - `document.getElementById`
 - `document.getElementsByClassName`
 - `document.getElementsByTagName`
 - `document.querySelector`
 - `document.querySelectorAll`
 - 
La plus simple est `document.getElementById`, il suffit d'indiquer l'id d'une balise html pour la récupérer
```javascript
const ma_balise = document.getElementById("ma_div")
console.log(ma_balise) // ma_balise contient un objet qui me permet de modifier la balise html correspondante
```

avec l'objet récupéré, je peut manipuler la balise HTML:
```javascript
//lire le texte dans une balise html
console.log(ma_balise.innerText)

//remplacer / ajouter du texte dans une balise html (ne permet pas d'ajouter des balises)
ma_balise.innerText = "Salut"
ma_balise.innerText += " à tous"

//remplacer / ajouter du texte+des balises dans une balise html
ma_balise.innerHTML += "<h1>Bonjour!!!</h1>"

// modfier une probiété css(en inline), ici le bacgound-color
ma_balise.style.backgroundColor="#00ff00"
```

Avec le dom, on peut aussi récupérer des informatins fournies par l'utilisateur, par example, avec un input, on peut lire la valeur écrite.
```javascript
mon_input = document.getElementById("un_input")

//permet de lire le contenu d'un champs de formulaire (ce que l'utilisateur à écrit dedans)
console.log(mon_input.value) // /!\ la case est vide au chargement de la page, il est donc probable de récuperer un resultat vide ici

// on peut aussi modifier le contenu
mon_input.value = "salut"

```

Enfin, on peut réagir à certaines actions (souvent celles de l'utilisateur maispas que), par example à un click ou l'appui d'une touche du clavier
pour celà on utilise un **Event listener**, il s'agit d'une fonction qui sera executée au moment souhaité grace à `addEventListener`



```javascript
// Il faut commencer par créer la fonction 
function ma_fonction(){
console.log("Salut!")
}

// permet de déclancher le code dans "ma_fonction" lorce que l'on appuie sur une touche du clavier
document.addEventListener("keydown", ma_fonction)


// permet de déclancher le code dans "ma_fonction" lorce que l'on clique SUR LA BALISE dans "ma_balise"
ma_balise.addEventListener("mousedown", ma_fonction)

// on peut aussi créer la fonction en même temps (si la fonction est assez courte, sinon, ce n'est plus lisible)
document.addEventListener("keydown", function (){
    console.log("j'ai cliqué!!");
})
```

Dans la fonction ([callback function](@/js/algo/#callback-functions)), on a accès à des détails supplémentaires concernant l'évènement.
Ils sont injectés en argument de la fonction sous forme d'objet, donc il suffit de créer un paramettre dans la fonction pour obtenir ces infos.
pour avoir le détail de ce que contient l'évènement, on peut l'afficher avec un `console.log` ou lire [la doc](https://developer.mozilla.org/en-US/docs/web/api/event#interfaces_based_on_event)

Example avec un [MouseEvent](https://developer.mozilla.org/en-US/docs/Web/API/MouseEvent) déclanché par un mouvement de souris:
```javascript
// Il faut commencer par créer la fonction 
function ma_fonction(event){
console.log(event) // affiche tout le contenu de l'évènement
console.log(event.x + " - " + event.y) // affiche les coordonnées de la souris
}

document.addEventListener("mousemove", ma_fonction)

```

