---
title: AJAX
---

## Intro

AJAX (Asynchronous Javascript And XML) permet d'échanger des informations avec un serveur "de manière asynchrone" en javascript.

Ajax utilise le protocole HTTP pour ces échanges (le même que le navigateur quand il charge de l'html, du css ou des images par ex)
Cela permet à javascript d'accéder à des informations stockées sur un serveur (infos de météo, autocompletion google, messages sur un chat)

Ajax permet de:

- avoir un site plus rapide et réactif, (le js et l'HTML est préchargé, il ne reste plus qu'à charger les informations beaucoup plus légères)
- avoir une application plus évolutive (les données json sont comprises par beaucoup plus de langages, par ex des logiciels ou app mobiles)
- fluidifie la navigation (pas besoin de recharger la page à chaque interaction)

il a aussi des inconvenients :

- temps de chargement total peut être plus long
- référencement moins bon si le contenu n'est pas dans la page au chargement, il ne sera pas vu par les bots de google

AJAX peut transférer des données dans n'importe quel format, mais le plus utilisé (et celui que nous verrons principalement) est JSON

Les requètes ajax peuvent envoyer ou recevoir des données, mais les demandes doivent toujours êtres initiées par le client (navigateur/js)

## Les APIs

Avec AJAX, on appel une API (Application Programming Interface), il s'agit simplement d'un serveur web qui renvoi des données pour un autre programme, souvent en JSON ou en XML

Une API peut être :
privée: codée spécifiquement pour un site et uniquement accessible par celui-ci
ex:

- l'api de gestion de pannier de mon site e-commerce,
- api du chat de mon site web
- l'api d'autocompletion de la barre de recherche google
- etc...

publique(gratuite/payante/semi-payante): met à disposition de tous des informations afin de les utiliser sur un autre site web, souvent contre rétribution
ex:

- API youtube
- api de geocoding du gouvernement (api.gouv.fr)
- api de paiement en ligne
- SWAPI

une petite liste de certaines API publiques:  
https://github.com/public-api-lists/public-api-lists

## Ajax en pratique

On peut faire des requetes AJAX de deux manières(Nativement):

- XMLHttpRequest(XHR), methode d'origine en javascript, existe depuis longtemps. Elle est compatible avec tout les navigateurs mais est plutôt longue à écrire.
  [Documentation xhr](https://developer.mozilla.org/fr-FR/docs/Web/API/XMLHttpRequest/Using_XMLHttpRequest)

- fetch qui est plus récent (pas compatible avec IE) mais beaucoup plus simple à utiliser.
  [Documentation fetch](https://developer.mozilla.org/fr/docs/Web/API/Fetch_API)

Il existe aussi un certain nombre de librairies js qui permettent d'utiliser ces deux fonction de manière differentes, par ex

- jquery (librairie tout en un qui comprend une fonction ajax) qui utilise la XMLHttpRequest
  [Documentation ajax avec jquery](https://api.jquery.com/Jquery.ajax/)

- Axios(-http) librairie qui utilise fetch pour faire des appels ajax
  [Documentation Axios](https://axios-http.com/docs/example)

## Le JSON

Le JSON est un format de données régulièrement utilisé dans les APIs.
Le format des fichiers JSON resemble à javascript, **mais ce n'en est pas**.
Cependant, il est très facile de transformer du JSON en JS et vice et versa, certaines librairies le font même automatiquement.

Example de JSON:

```json
{
  "fname": "John",
  "lname": "Smith",
  "animals": ["Canigoux", "Boule de poils", "Petit papa noel 2"]
}
```

Le JSON à cependant certaines spécificités, dont:

- les clés dans les objets sont toujours entre guillemets
- les guillemets sont toujours doubles
- il n'est pas possible de laisser une virgule supplémentaire à la fin d'un tableau / objet

## Protocole HTTP

Le protocole HTTP est la technologie qui régi tous les échanges sur le web (chargement de page web, d'assets, envoie de formulaire, appels ajax).
Il est particulièrement important de le maitriser lorsque l'on utilise une API, puisque la plus-part utilisent toutes les fonctionnalités du protocole.

HTTP est un protocole **client-serveur**, ceci signifie qu'un programme va faire une demande(**Requête**) à un serveur qui lui renverra un résultat(**Réponse**).

### Requête HTTP

#### méthodes HTTP:

La méthode HTTP est une information envoyée avec chaque requête, elle pourra être prise en compte par le serveur pour changer l'action à réaliser.
Sur une API, la méthode est souvent utilisée afin d'effectuer plusieures actions différentes sur la même url, par example:

- `https://example.com/api/articles` en `GET` enverra la liste des articles
- `https://example.com/api/articles` en `POST` Permettra de créer un article

Sur le web, dans la plupart des cas, les requêtes utilisent la méthode `GET`

`GET` => Méthode HTTP

Liste des méthodes:  
Méthodes qui permettent d'envoyer un contenu

- `POST`
- `PUT`
- `PATCH`

ne permettent pas d'envoyer un contenu

- `GET`
- `DELETE`

#### URL

L'URL est aussi une information qui pourra être lue et prise en compte par le serveur.  
Elle est composée de plusieures parties.

`https://swapi.dev/api/people/1?search=bonjour&sort=title` => URL

- `https://` => protocole
- `swapi.dev` => nom de domaine
- `/api/people/1` => route/chemin
  - `1` => Path Parameter (paramètre de chemin)
- `?search=bonjour&sort=title` => Query parameters

#### headers

Les headers son de 'méta données' envoyées avec chaque requête.
Par défaut, certains headers sont mis automatiquement par le navigateur,
par ex: cookies, user-agent, Accept-Language, ...

Je vais pouvoir dans mes requêtes ajax définir des headers supplémentaires,
comme par example x-api-key pour envoyer une clé d'api.

#### Body (content)

Contenu (corp) de la requête, ce sont les données "principales" de la requête.
On ne peut envoyer de body qu'avec des méthodes `POST`, `PUT`, `PATCH`
Le contenu de la requête est du texte, on peut y envoyer ce que l'on veut.
Cependant, on utilisera souvent des formats de données connus comme json ou xml

### Réponse

Une réponse aura des headers et un body (voir requête)

## Protocole HTTP avec axios

Examples de gestion des champs http dans une requête HTTP avec axios

```js
// méthode http
axios.get(
  "https://example.com/api/test-route", // url à appeler
  {
    // configuration de la requête
    params: {
      // définition des query params
      api_key: key,
      test: "J'aime le chocolat",
    },
    headers: {
      // définition des headers
      test2: "j'aime aussi les tartes aux fraises",
      ma_cle: "une-cle-super-secrete",
    },
  }
);

axios.post(
  "https://example.com/api/test-route",
  // en post/put/patch, on rajoute le body ici
  //
  // si on souhaite ajouter des headers ou des query params,
  // on doit obligatoirement le renseigner, même si il est vide
  // (on peut alors lui donner null en valeur)
  {
    field_test: "une valeur",
  },
  {
    params: {
      api_key: key,
      message_test: "J'aime le chocolat",
    },
    headers: {
      headerTest: "j'aime aussi les tartes aux fraises",
      ma_cle: "une-cle-super-secrete",
    },
  }
);
```

## Normes REST

REST est un type spécifique d'API qui respecte un certain nombre de normes.
En générale elle communique des informations en JSON (même si il reste possible de créer une API REST utilisatn du XML ou d'autres formats de données).
Les API REST sont fortement basées sur le protocole HTTP, elles font correspondre chaque action à une url avec une méthode HTTP.

Les routes doivent suivre les méthode HTTP:

- `GET`: récupération de données
- `POST`: Ajout d'information
- `PATCH`: modification d'informations
- `PUT`: remplacement d'informations
- `DELETE`: suppression d'informations

Chaque route tout concerner une entité ou une collection d'entité, on viendra les regrouper par l'entité qu'elles concernent

Example de BREAD pour une entité article dans une API REST

| BREAD      | Description                  | Méthode HTTP | URL               |
| ---------- | ---------------------------- | ------------ | ----------------- |
| **B**rowse | Liste des article            | GET          | /api/articles     |
| **R**ead   | Lecture d'un article         | GET          | /api/articles/:id |
| **E**dit   | Modification d'un article    | PUT/PATCH    | /api/articles/:id |
| **A**dd    | Création d'un nouvel article | POST         | /api/articles     |
| **D**elete | Suppression d'un article     | DELETE       | /api/articles/:id |

Une API REST peut contenir tout ou partie des actions BREAD / CRUD mais aussi des routes spécifiques métier, il reste cependant important de respecter les norme de nommage des URLs et les actions HTTP


Une API REST doit aussi être stateless, les appels aux différentes routes doivent être indépendants et ne pas nécéssiter de concerver des informations de session pour fonctionner
