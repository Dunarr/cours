---
title: Introduction aux Frameworks Web
---

## 1. Introduction aux Frameworks Web

### Définition et rôle des frameworks
- Un framework est un ensemble d'outils et de bonnes pratiques facilitant le développement d'applications.
- Il fournit une structure standardisée pour le code et des fonctionnalités prêtes à l'emploi.
- **Rôle** : simplifier les tâches courantes comme la gestion des routes, l'accès aux bases de données, ou encore l'authentification.
- **Sécurité** : De nombreux frameworks incluent des mécanismes intégrés pour protéger les applications (prévention des injections SQL, protection contre les failles XSS, gestion des sessions).
- **Reproductibilité/portabilité**: Les frameworks utilisent plusieurs méthodes standard afin de facilité le lancement d'une application sur plusieurs environnements:
  - Gestion des librairies externes via un gestionnaire de paquets.
  - Couches d'abstraction via des librairies qui permettent de rendre le code portable dans des environnements différents.
  - Configuration, via des variables d'environnement afin de changer le comportement dans des environnements différents et ne pas partager des secrets
  - Migrations pour sauvegarder, partager et historiser les structures des données.

### Différence entre librairie et framework
- Une **librairie** est un ensemble de fonctions ou d'outils qu'un développeur peut appeler selon ses besoins.
- Un **framework** impose une structure et un flux de travail, et le développeur s'y intègre.

### Différence entre frameworks Frontend et Backend
- **Frontend** : Concerne l'interface utilisateur (React, Angular, Vue.js). Suit la structure MVVM.
- **Backend** : Concerne la gestion du serveur et des données (Express.js, Django, Laravel, Rails, ASP). Suit la structure MVC

## 2. Concepts fondamentaux

### Architecture MVC
- **Modèle** : Gestion des données et logique métier.
- **Vue** : Affichage des données.
- **Contrôleur** : Traitement des requêtes et communication entre Modèle et Vue.

**Schéma** :
```
Requête Utilisateur -> Contrôleur -> Modèle -> Vue -> Réponse Utilisateur
```


### Architecture MVVM
- **Modèle** : Gestion des données et logique métier (identique au MVC).
- **Vue** : Interface utilisateur (UI).
- **Vue-Modèle (ViewModel)** : Fait le lien entre le Modèle et la Vue en gérant la logique de présentation.
    - Exemple : Mise à jour automatique de l'interface utilisateur lorsque les données changent grâce à des mécanismes de liaison de données (data binding).


**Schéma** :
```
Requête Utilisateur -> Vue -> Vue-Modèle -> Modèle -> Vue-Modèle -> Vue -> Réponse Utilisateur
```


### Gestion des routes
- Associe une URL à une action spécifique.
- Exemple avec Express.js :
  ```javascript
  app.get('/home', (req, res) => {
    res.send('Bienvenue sur la page d\'accueil');
  });
  ```


### Structure typique des projets

- **Frontend** :
  ```
  dist/           # Dossier généré à la compilation du projet, contient le code compilé dans un format lisible par le navigateur (HTML/CSS/JS).
  node_modules/   # Dossier contenant les dépendances installées via un gestionnaire de paquets comme npm ou yarn.
  src/            # Dossier principal où le code source est organisé.
  ├── components/ # Contient des composants réutilisables, souvent des morceaux d'interface utilisateur comme des boutons, formulaires, etc.
  ├── views/      # Contient les différentes pages ou vues de l'application.
  ├── assets/     # Contient les ressources statiques comme les images, polices, fichiers CSS personnalisés, etc.
  ├── main.js     # Point d'entrée principal du projet côté frontend, où l'application est initialisée.
  package.json    # Fichier contenant les métadonnées du projet ainsi que les scripts et dépendances.
  .env            # Fichier de configuration pour les variables d'environnement.
  ```

- **Backend** :
  ```
  config/         # Contient les fichiers de configuration du projet (ex. paramètres de connexion à la base de données, réglages des services, etc.).
  migrations/     # Contient les scripts de migration pour gérer la structure de la base de données (création, modification des tables).
  node_modules/   # (ou vendor/ pour PHP) Dossier contenant les dépendances installées via un gestionnaire de paquets comme npm ou Composer.
  public/         # Contient les fichiers accessibles publiquement, comme les images, styles, scripts ou le point d'entrée public (index.php/index.html).
  src/            # Dossier principal du code source backend.
  ├── controllers/ # Contient les fichiers qui gèrent la logique de traitement des requêtes et des réponses.
  ├── models/      # Contient les définitions des structures de données et leur interaction avec la base de données.
  .env            # Fichier de configuration pour les variables d'environnement (ex. clés API, URL de la base de données).
  package.json    # (ou composer.json) Fichier contenant les métadonnées du projet ainsi que les scripts et dépendances.
  ```


## Ressources complémentaires
- [Documentation React](https://reactjs.org/)
- [Documentation Angular](https://angular.io/)
- [Documentation Vue.js](https://vuejs.org/)
- [Documentation Express.js](https://expressjs.com/)
- [Documentation Django](https://www.djangoproject.com/)
- [Documentation Laravel](https://laravel.com/)
- [Documentation Symfony](https://symfony.com/)
- [Documentation Symfony](https://symfony.com/)
- [Documentation Sails](https://sailsjs.com/)
- [Documentation adonis](https://docs.adonisjs.com/)
