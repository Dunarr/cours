---
title: Intro maintenance d'application et sécurité
---

## Types de failles principales
Lorsque l'on fait un site web, il est très facile d'avoir des failles de sécuritées qui peuvent
impacter le fonctionnement du site.
Il est donc nécéssaire de les prendre en compte lors de la concéption
et du développement du site pour les éviter.
Voici quelques examples de failles communes sur les sites web
- failles XSS:
  permettent aux utilisateur d'envoyer / injecter de l'html voir du js
  sur le site, celui-ci sera affiché à tout les utilisateur et peut leur nuire
  on peut principallement les retrouver sur les forum/chat/commentaires
- injections (SQL):
  il s'agit de failles qui permettent à un utilisateur d'executer
  du code coté serveur en l'envoyant dans un message, il peut s'agire d'injection SQL ou autre (Log4J)
  cela se produit lorsque l'on controle un langage avec un autre (sql dans php, log dans java, etc)
  et peut être exploité par example dans des formulaires
- csrf
  consiste à envoyer un lien / formulaire à une personne (A)
  le lien pointe sur un site ou A est administrateur, donc si il clique dessus,
  et qu'il arrive (par example) sur la page de supression des compte, l'action sera executé.

Toutes ces failles peuvent impacter mon site à different niveau, soit en affectant les utilisateurs
soit en affectant directement le serveur et le site lui même. Enfin cela peut aussi entrainer des fuites de données.


## Mises à jour
lorsque l'on utilise des librairies ou des outils externes (par ex, des CMS),
il faut régulièrement les metre à jour, et effectuer de la veille afin d'être au courrant
des failles qui sont découvertes. Il faut aussi (si possible)
limiter les librairies / plugins pour diminuer les failles potentielles.

## Les sauvegardes
Même si elles ne permettent pas de ser prémunire des attaques, elles permettent de remettre nos outils en état.
Que ce soit après une attaque ou même des choses plus aléatoires (incendie, destruction, mauvaise manipulation)
Les backups permettent de sauvegarder toutes les données du site. Il y a quelques règls à suivre pour les sauvegarde:
- Les sauvegardes sont aussi importantes que le site en lui même, il faut donc autant les sécuriser.
- IL est préférable de ne pas mettre les sauvegarde au même endroit que l'application pour ne pas perdre les deux en même temps

## Politique de sécurité
L'une des failles majeures dasn les SI(Systèmes d'informations) est le facteur humain. En effet un site très sécurisé,
mais dont le compte administrateur a un mot de passe faible sera très facilement hackable (aujourd'hui, presque tout les sites worrdpress
en ligne ont au moins une fois été fictime de Brute Force, tentative de connexion répété avec des mots de passe pour trouver le bon).

Pour éviter ce soucis, on peut mettre en place des politique de conditions minimales pour les mots de passe
(un mot de passe avec chiffres, lettres et symboles de moins de 10 caractères met trois semaines à être trouvé).
On peut aussi sir un site forcer l'utilisation d'un "second facteur d'authentification":
- validation par SMS / Mail
- code unique généré sur un téléphone
- [yubikey](https://www.yubico.com/)

Pour implémenter ces sécuritées dans des sites, il existe des plugins / librairies, par example  pour wordpress:
- updraftplus => backups distants des sites
- wp-2fa => activation d'un second facteur d'authentification

Il existe d'autres outils externes pour protéger un site comme:
- cloudflare => anti DDOS / proxy
- outils de backups fournis par les hébergeurs

Pour eux qui veulent aller pluss loin et se tenir au courrant sur les failles decouvertes, voici quelques sites:
- https://owasp.org/
- https://securite.developpez.com/
- https://wordpress.org/news/

