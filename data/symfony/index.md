---
title: Symfony
---

## Intro

Symfony est un framework MVC PHP
Comme les autress frameworks, il apporte une suite d'outils (librairies) pour simplifier le développement. Il apporte
aussi un cadre pour coder, grace à une structure de dossier prédéfinie, un découpage des fonctionnalitées dans des
fichier dédiés et des normes de syntaxe.

Les principaux frameworks MVC PHP sont:

- laravel (le plus utilisé dans le mode)
- symfony (français, et le plus utilisé en france)
- cakephp

La grande majorité des langages backend possèdent des frameworks MVC
 - node: nest,  sails, ...
 - Python: Django
 - Ruby: Rails 

La structure et les éléments des frameworks sont similaires de l'un à l'autre

## MVC

MVC est l'accronyme de Model Vue Controlleur, celà représente les trois parties principales de ce type d'applications.

### Le model:

Correspond à la partie gestion des données, elle va entre-autres gérer les interacions avec la bdd. Sur Symfony, c'est
la librairie doctrine qui est en charge de cette partie

### La vue:

Correspond à l'affichage des informations. En effet, l'affichage des informations pour l'utilisateur est découplée de la
logique et se contente d'afficher les résultats des opérations effectuées en avance.

### Le controlleur

Correspond à la brique centralle de l'application, en fonction de la requète de l'utilisateur(url, méthode http, données
de formulaire, ...) elle va déterminer quelle action mener, récupérer les informations du modèle, appliquer la logique
metier et envoyer les résultats à la vue pour générer la réponse à envoyer.

## Installation

Symfony nécéssite plusieurs logiciels pour fonctionner:

- php (version suivant la version de symfony)
- composer [(guide d'installation)](https://getcomposer.org/download/)
- symfony cli [(guide d'installation)](https://symfony.com/download/)

On peut ensuite créer un projet avec la commande:

```shell
symfony new NOM_DOSSIER_A_CREER
```

## Variables d'environement

Les variables d'environement sont des paramètres que l'on peut configurer pour définir le comportement de l'application
et fournir les secrets (token, mot de passe...) à l'application
Elles sont spécifique à chaque copie du projet et peuvent varier d'un ordinateur à l'autre
Pour configurer les variables d'environnement sur un environnement (pc), il faut créer un fichier `.env.local` à la
racine du projet
On peut copier le contenu du `.env` dans le `.env.local` pour l'utiliser comme base.
Toutes les variables à configurer sont indiquées dans le fichier `.env`

## Lancement du projet

Pour lancer / accéder au projet, il faut utiliser le serveur integré de symfony.
Pour celà, dans le dossier du projet, lancer:

```shell
symfony serve
```

## Création de page

### Controlleurs

Sur symfony toutes les pages (**routes**) sont définies dans des méthodes de controlleurs.
Les controlleurs sonts des classes paritculières qui se retrouvent dans le dosser src/Controller

```php
<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class MainController extends AbstractController
{
    #[Route('/home', name: 'homepage')] // <======== attribut PHP qui permet d'indiquer que la méthode correspond à la route /home
    public function index(): Response
    {
        return new Response('Hello World'); // <======= une route symfony retourne toujours une réponse,
    }
}
```

### Templates

Afin d'afficher du contenu, on peut utiliser le package twig afin de créer des templates pour les pages

```shell
composer require twig
```

Avec le package twig, on peut écrire le contenu HTML de nos pages dans des fichiers de templates tw!ig et les utiliser
dans les routes de controlleur.

```php
<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class MainController extends AbstractController
{
   //.... 
    #[Route("/prise-de-contact", name: "page_contact")]
    public function pageContact(): Response
    {
        // ici on utilise un template twig (templates/contact/index.html.twig) pour afficher la page
        return $this->render("contact/index.html.twig");
    }
}
```

On peut générer un controlleur, une route et son template grace au package maker

Installation du package maker

```shell
composer require make
```

Génération du controlleur

```shell
php bin/console make:controller
```

## Interaction avec la bdd (ORM)

Sur les frameworks MVC, on ne manipule pas directement la base de données,
on construit notre application avec des classes spéciales (entitées)
qui représentent les tables en base de donnée.

Cela change la manière dont on travialle avec la bdd,
par ex, pour la conception, on pourra directement faire un diagramme de classes.
On pourra ainsi créer nos entitées et la bdd en découlera.

Pour celà, on utilise une librairie de type ORM (Object Relational Mapping).
Pour symfony, l'ORM est doctrine, pour l'installer, il faut faire:

```shell
composer require orm
```

Il faut ensuite configurer les information pour connecter le projet Symfony au SGBD.
Pour celà, il faut renseigner la variable `DATABASE_URL` dans le fichier `.env.local`

```dotenv
DATABASE_URL="mysql://USERNAME:PASSWORD@127.0.0.1:3306/DBNAME?serverVersion=MARIADBVERSION-MariaDB&charset=utf8mb4"
```

Enfin, on peut demander à symfony de créer la base de donnée correspondant aux paramètres renseignés avec:

```shell
php bin/console doctrine:database:create
```

On peut générer/modifier une entité avec la commande

```shell
php bin/console make:entity
```

lorsque l'on apporte des modifications à nos entités,
on doit ensuite modifier la base de données en passant par les migrationss.
On commence par créer la migration pour générer le sql avec:

```shell
php bin/console make:migration
```

ensuite, on l'applique avec:

```shell
php bin/console doctrine:migrations:migrate
```

### récupération en Bdd

Tout les échanges avel la bdd se faut avec le repository (classe générée automatiquement avec chaque entité).
On peut le récupérer en paramètre dans le controlleur, ex:

```php
class ArticleController extends AbstractController
{ 
    #[Route('/article', name: 'app_article')]
    public function index(ArticleRepository $articleRepository): Response
    {
      //....
    }
}
```

Pour récupérer des informations en bdd, on peut utiliser les méthodes du repository corresponant à l'entité souhaitée:

- find
- findAll
- findBy
- findOneBy

example d'utilisation de findOneBy:

```php
$articleRepository->findoneBy(["slug" => "mon-super-article"]);
$articleRepository->findoneBy(["category" => "people"], ['postedAt'=>'ASC'], 10); // retourne les 10 les plus anciens de la catégorie 'people'
```

find et findOneBy retournent une entité (ou null si il n'y a pas de résultat)   
findAll et findBy retournent un tableau d'entités (tableau vide si il n'y a pas de résultat)

#### Example de route pour l'affichage d'une entité
```php

<?php

namespace App\Controller;

use App\Entity\Article;
use App\Entity\Film;
use App\Repository\ArticleRepository;
use App\Repository\FilmRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;

class FilmsController extends AbstractController
{
    #[Route('/film/{id}', name: 'app_film_show')]
    public function filmShow(int $id, FilmRepository $filmRepository): Response
    {
        $film = $filmRepository->find($id);
        if ($film === null) {
            throw $this->createNotFoundException();
        }
        return $this->render('films/show.html.twig', [
            'film' => $film
        ]);
    }
    
    // symfony peut aussi retrouver automatiquement une entité en utilisant l'injection de dépendance (il fait un findBy sur le nom du parametre dans l'url)
    #[Route('/article/{id}', name: 'app_article_show')]
    public function articleShow(Article $article): Response
    {
        return $this->render('/article/show.html.twig', ['article' => $article]);
    }

```

### Création et modification d'entité

Pour créer une entitée, on la créé comme un objet classique (avec le mot clé new)
puis on la sauvegarde à l'aide des methodes persist et flush de l'entity manager
exemple:

```php
class ArticleController extends AbstractController
{ 
    #[Route('/article/new', name: 'app_article_new')]
    public function new(EntityManagerInterface $entityManager): Response
    {
      $article = new Article();
      $entityManager->persist($article); // indique que l'on souhaite sauvegarder l'entité
      $entityManager->flush($article); // applique tout les changement en BDD
      //....
    }
}
```

Si l'entité est déjà existant (si elle vient de la BDD), on peut sauvegarder toutes les modifications qui lui ont été
apporté avec flush
exemple:
```php
class ArticleController extends AbstractController
{ 
    #[Route('/article/edit', name: 'app_article_edit')]
    public function new(EntityManagerInterface $entityManager, ArticleRepository $articleRepository): Response
    {
      // Il faut récupérer l'entité à modifier (Il y a plusieures méthodes possibles)
      $article = $articleRepository->find(5);
      if($article){
        $article->setTitle('Nouveau titre');
        $entityManager->flush(); // applique tout les changement en BDD (pour $article et toutes les autres entités qui ont pu être modifiées)
      }
      //....
    }
}
```

### Suppression d'une entité

L'entity manager de Doctrine peut supprimer une entité (et donc l'enregistrement correspondant en BDD).   
Pour celà, il faut utiliser la méthode remove, puis flush.

Example:

```php
class ArticleController extends AbstractController
{ 
    #[Route('/article/edit', name: 'app_article_edit')]
    public function new(EntityManagerInterface $entityManager, ArticleRepository $articleRepository): Response
    {
    // Il faut récupérer l'entité à supprimer (Il y a plusieures méthodes possibles)
      $article = $articleRepository->find(5);
      if($article){
        $entityManager->remove($article); // Supprime l'article (non effectif tant qu'on a pas flush)
        $entityManager->flush(); // applique tout les changement en BDD (Supprime l'article)
      }
      //....
    }
}
```


## Formulaires
Symfony propose une gestion des formulaires fia le package `symfony/security`. Il s'installe avec:
```shell
composer require twig
```

### Création de formulaire
La structure du formulaire est gérée via une classe. Cette classe peut être générée via le maker
```bash
php bin/console make:form
```
Cette commande vous invite à donner un nom à votre formulaire et à indiquer s'il doit être lié à une entité.

### Configuration d'un formulaire 
La classe d'un formulaire contient deux methodes: `buildForm` et `configureOptions`

Dans la methode buildForm, on a accès au paramètre $builder(FormBuilderInterface)
dont la methode add permet d'ajouter des champs au formulaire
la methode add reçoit trois paramètres:
 - `child`: Le nom machine de l'input (equivalent du name html), pour un formulaire lié a une entité, il doit être égal au nom de l'attribut correspondant dans la classe
 - `type`: La classe de l'input, afin de choisir son type (voir la documentation symfony pour la liste des types disponnibles) 
 - `options`: Les options de l'input (label, classes css, requis ou non, ...) sous forme de tableau associatif, chaque type d'input a des options différentes (voir la documentation symfony pour la liste des options pour chaque type disponnible)

```php
// src/Form/ContactType.php
namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ContactType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('name', TextType::class, [
                'label' => 'Nom',
                'required' => true,
            ])
            ->add('email', EmailType::class, [
                'label' => 'Adresse Email',
                'required' => true,
            ])
            ->add('message', TextareaType::class, [
                'label' => 'Message',
                'required' => true,
            ])
            ->add('submit', SubmitType::class, [
                'label' => 'Envoyer',
            ]);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            // Pas d'entité liée ici, donc on peut laisser vide ou utiliser un tableau vide.
            'data_class' => null,
        ]);
    }
}
```

### Instantiation du formulaire et manipulation des données
Le formulaire doit être instantié dans une route de controlleur à l'aide de la methode `createForm`
Il peut ensuite être hydraté des données envoyées par l'utilisateur afin de les utiliser.
Pour afficher le formulaire, il faut le fournir en paramêtre au template

```php
// src/Controller/ContactController.php
namespace App\Controller;

use App\Form\ContactType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ContactController extends AbstractController
{
    #[Route('/contact', name: 'contact')]
    public function contact(Request $request): Response
    {
        // Crée le formulaire
        $form = $this->createForm(ContactType::class);

        // Traite la requête HTTP, lie les données envoyées par l'utilisateur aux champs du formulaire
        $form->handleRequest($request);

        // Si le formulaire est soumis et valide
        if ($form->isSubmitted() && $form->isValid()) {
            // Récupère toutes les données du formulaire sous forme de tableau associatif
            $data = $form->getData();

            // Exemple d'utilisation des données individuelles
            $name = $data['name'];
            $email = $data['email'];
            $message = $data['message'];

            // Simuler une action, comme l'envoi d'un email
          

            // Redirection de l'utilisateur vers une autre page afin d'éviter que le formulaire soit envoyé plusieurs fois de suite.
            return $this->redirectToRoute('contact');
        }

        // Rendu de la vue avec le formulaire
        return $this->render('contact.html.twig', [
            'form' => $form,
        ]);
    }
}
```

### Rendu du formulaire
Le formulaire peut être affiché dans un template twig à l'aide de la fonction `form` 
```html
{# templates/contact.html.twig #}
{% extends 'base.html.twig' %}

{% block body %}
    <h1>Contactez-nous</h1>
    {{ form(form) }}
{% endblock %}
```
Il est aussi possible d'afficher les inputs un par un afin de personaliser le rendu HTML
```html
{# templates/contact.html.twig #}
{% extends 'base.html.twig' %}

{% block body %}
    <h1>Contactez-nous</h1>

    {{ form_start(form) }}
        {{ form_row(form.name) }}
        {{ form_row(form.email) }}
        {{ form_row(form.message) }}
        {{ form_row(form.submit) }}
    {{ form_end(form) }}
{% endblock %}
```


### Formulaire lié à une entité
Il est possible de lier une entité à un formulaire afin de facilement modifier ou créer une instance:

```php
namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use App\Entity\Article;

class ArticleType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        // Les champs du formulaire sont liés aux attributs de l'entité et viendront la remplire. 
        $builder
            ->add('title', TextType::class, [
                'label' => 'Titre de l\'article',
                'required' => true,
            ])
            ->add('content', TextareaType::class, [
                'label' => 'Contenu',
            ]);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        // L'option data_class permet de lier une entité au formulaire
        $resolver->setDefaults([
            'data_class' => Article::class,
        ]);
    }
}
```
```php
namespace App\Controller;

use App\Entity\Article;
use App\Form\ArticleType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Doctrine\ORM\EntityManagerInterface;

class ArticleController extends AbstractController
{
    public function new(Request $request, EntityManagerInterface $em): Response
    {

        $form = $this->createForm(ArticleType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            // ici, la methode getData() retourne directement une instance de la classe indiquée en "data_class" soit un Article
            $article = $form->getData();
            // Il suffit donc de persister l'article avec doctrine
            $em->persist($article);
            $em->flush();
            return $this->redirectToRoute('article_list');
        }

        return $this->render('article/new.html.twig', [
            'form' => $form,
        ]);
    }
}
```

## Gestion de l'authentification sur Symfony

### Notions générales

#### Compte utilisateur

Un compte utilisateur représente les informations permettant d'identifier une personne au sein de l'application, Dans la majorité des cas, ils sant sauvegardés en base de données.
Ces informations incluent généralement :

- **Nom d'utilisateur** (ou email) : utilisé pour se connecter.
- **Mot de passe** : sécurisé, doit être stocké sous forme de hash dans la base de données.
- **Rôles** : définissent les permissions d'accès (ex. : `ROLE_USER`, `ROLE_ADMIN`).

#### Formulaire de connexion

Un formulaire de connexion permet aux utilisateurs de s'authentifier en saisissant leurs identifiants (nom
d'utilisateur/email et mot de passe), on compare alors les données envoyées aux comptes stockés dans l'application

#### Session de connexion

Lorsqu'un utilisateur est authentifié, une session est créée pour stocker son état de connexion (généralement un cookie
de session). Cela évite de demander ses identifiants à chaque requête.

#### Gestion d'accès

La gestion d'accès consiste à contrôler quelles ressources ou pages de l'application sont accessibles à un utilisateur
en fonction de ses rôles, permissions ou du "propriétaire" des données. Il peut aussi s'agir de limiter le contenu visible dans la page.

#### Rôles

Les rôles permettent de définir des groupes d'autorisation. Par exemple :

- `ROLE_USER` : accès basique pour un utilisateur authentifié.
- `ROLE_ADMIN` : accès aux fonctionnalités d'administration.

(Il n'y a pas de rôles prédéfini dans symfony Il revient au développeur de créer et gérer les droits pour chacun des rôles souhaités)

---

### Installation du composant de sécurité de Symfony

Symfony fournit un composant de sécurité intégré pour gérer l'authentification et les autorisations.

#### Étapes d'installation

1. **Installer le bundle de sécurité Symfony** (souvent inclus par défaut) :

```bash
composer require security
```

2. **Configurer la sécurité dans le fichier `config/packages/security.yaml`**.

---

### Configuration principale du fichier `security.yaml`

Le fichier `security.yaml` contient les paramètres principaux pour gérer l'authentification et l'autorisation.
Le contenu par défaut est généré lors de l'installation du bundle et est modifié automatiquement par le maker.
Il est également possible de le modifier manuellement
Voici un exemple minimal :

```yaml
security:
  password_hashers:
    Symfony\Component\Security\Core\User\PasswordAuthenticatedUserInterface: 'auto'
  providers:
    app_user_provider:
      entity:
        class: App\Entity\User
        property: username
  firewalls:
    dev:
      pattern: ^/(_(profiler|wdt)|css|images|js)/
      security: false
    main:
      lazy: true
      provider: app_user_provider
      form_login:
        login_path: app_login
        check_path: app_login
        enable_csrf: true
      logout:
        path: app_logout


  access_control:
   - { path: ^/login, roles: PUBLIC_ACCESS }
   - { path: ^/admin, roles: ROLE_ADMIN }
   - { path: ^/, roles: ROLE_USER }
```

#### Explications des sections principales :

- **`password_hashers`** : Configure l'algorithme de hachage des mots de passe.
- **`providers`** : Définit comment les utilisateurs sont chargés (ici depuis une entité Doctrine `User`).
- **`firewalls`** : Configure les méthodes d'authentification (formulaire de connexion, token, etc.).
- **`access_control`** : Règles d'accès basées sur des chemins et des rôles.

---

### Génération d'un utilisateur et d'un formulaire de connexion

Symfony fournit des commandes pour générer les fichiers nécessaires :

#### Générer une classe d'utilisateur

```bash
php bin/console make:user
```

Cela génère une entité `User` qui implémente les interfaces nécessaires:

```php
namespace App\Entity;

use App\Repository\UserRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\PasswordAuthenticatedUserInterface;
use Symfony\Component\Security\Core\User\UserInterface;

#[ORM\Entity(repositoryClass: UserRepository::class)]
#[ORM\UniqueConstraint(name: 'UNIQ_IDENTIFIER_USERNAME', fields: ['username'])]
class User implements UserInterface, PasswordAuthenticatedUserInterface
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 180)]
    private ?string $username = null;

    /**
     * @var list<string> The user roles
     */
    #[ORM\Column]
    private array $roles = [];

    /**
     * @var string The hashed password
     */
    #[ORM\Column]
    private ?string $password = null;

    // Getters et setters ici
}
```

Il s'agit d'une entité doctrine comme les autres, on peut donc y ajouter des attributs et la lier à d'autres entités.
Il faut aussi penser à générer et éxécuter les migrations pour créer la table correspondante en base de données.

#### Générer un formulaire de connexion

```bash
php bin/console make:security:form-login
```

Cela génère :

- Un contrôleur de sécurité.
- Un formulaire de connexion.
- une configuration dans le security.yaml

---

### Gestion d'accès avec `access_control` et `IsGranted`

#### Exemple avec `access_control`

Dans `security.yaml`, vous pouvez définir des règles d'accès globales :

```yaml
security:
  access_control:
    - { path: ^/admin, roles: ROLE_ADMIN }
    - { path: ^/profile, roles: ROLE_USER }
```

- Les chemins commençant par `/admin` sont réservés aux utilisateurs avec le rôle `ROLE_ADMIN`.
- Les chemins commençant par `/profile` sont réservés aux utilisateurs authentifiés avec le rôle `ROLE_USER`.

#### Exemple avec l'attribut `IsGranted`

Dans un contrôleur, utilisez l'attribut pour restreindre l'accès à une méthode :

```php
namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;

class AdminController extends AbstractController
{
    #[Route('/admin', name: 'admin_dashboard')]
    #[IsGranted('ROLE_ADMIN')]
    public function dashboard(): Response
    {
        return $this->render('admin/dashboard.html.twig');
    }
}
```

---

### Gestion de permissions dans un contrôleur

Dans un contrôleur, vous pouvez vérifier les permissions dynamiquement afin de modifier le comportement de la route en fonction :

#### Vérifier les rôles avec `isGranted`

```php
namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ProfileController extends AbstractController
{
    #[Route('/profile', name: 'profile')]
    public function index(): Response
    {
        if (!$this->isGranted('ROLE_USER')) {
            throw $this->createAccessDeniedException('Vous n\'avez pas accès à cette section.');
        }

        return $this->render('profile/index.html.twig', [
            'user' => $this->getUser(),
        ]);
    }
}
```

#### Obtenir l'utilisateur courant avec `getUser`

Enfin, il est possible de récupérer l'objet utilisateur correspondant à la session authentifiée afin d'afficher des informations contextuelles.

```php
$currentUser = $this->getUser();
if ($currentUser) {
    // Utilisateur authentifié
    $email = $currentUser->getEmail();
}
```





## packages pour symfony

Dans un nouveau projet, Symfony n'installeque les librairies necessaires pour créer un site minimal.
Pour ajouter des fonctionnalités, il faut installer les librairies correspondantes dans le projet.
Les librairies peuvent être retrouvées sur packagist.org.

Installer une librairie PHP (dans le dossier du projet)

```shell
composer require NOM_LIBRAIRIE
```

Les librairies principalles de Symfony ont des alias pour les retrouver plus rapidement.
Principalles librairies utiles sur Symfony:

- `twig`: Templates pour l'affichage des pages
- `debug` + `profiler`: outils de débuggage (affichage des erreurs, dump, informations diverses sur les pages chargées)
- `make`: Génération de code dans le projet via la console symfony
- `orm`: ORM (Doctrine), connexion à la base de données
- `form` + `validator`: Gestion des formulaires (HTML + validation + lien avec les entités)
- `security`: Gestion de l'authentification (utilisateur, cookies, gestion de droits)
- `mailer`: envoie de mails
- `cors`: gestion des CORS (pour la création d'une API publique)

## Bibliographie
### Installation de Symfony
- https://symfony.com/download

### Création d'un projet Symfony
- https://symfony.com/doc/current/setup.html

### Cours officiels de Symfony
- https://symfonycasts.com/courses#tracks

### Livre officiel pour bien commencer Symfony
- https://symfony.com/doc/6.4/the-fast-track/en/index.html

### Documentation de Symfony
- https://symfony.com/doc/current/index.html

### Documentation de Twig (templates)
- https://twig.symfony.com/doc/3.x/templates.html

### Documentation Doctrine (ORM)
- https://www.doctrine-project.org/projects/doctrine-orm/en/3.0/reference/basic-mapping.html#basic-mapping
- https://www.doctrine-project.org/projects/doctrine-orm/en/3.0/reference/association-mapping.html#association-mapping
- https://www.doctrine-project.org/projects/doctrine-orm/en/3.0/reference/inheritance-mapping.html#inheritance-mapping
- https://www.doctrine-project.org/projects/doctrine-orm/en/3.0/reference/working-with-objects.html#working-with-objects
- https://www.doctrine-project.org/projects/doctrine-orm/en/3.0/reference/working-with-associations.html#working-with-associations