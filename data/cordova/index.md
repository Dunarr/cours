---
title: Cordova
---

## Création d'une app
Cordova est une technologie qui permet de développer des applications mobile en convertisant un site web (en HTML, CSS, JS).
en  application en l'encapsulant dans un navigateur web

## Installation
- `npm install -g cordova` => installe l'utilitaire cordova
- `cordova create NOM_DOSSIER` => créé un projet cordova

ajout de plateformes
- `cordova platforme add android`
    - `cordova platforme add ios` => pour iphone
    - `cordova platforme add windows`
    - `cordova platforme add browser`

lancer l'application (sur un appareil connecté ou un emulateur):
- `cordova run NOM_PLATEFORME`


## Dépendences android
- java 8 (JDK + JRE)
- android studio
    - android sdk (tools > sdk manager)
        - dans SDK platform, installer la version qui nous interesse
        - dans SDK tools, installer SDK Build-Tools et SDK Platform-Tools

## Tester l'app

Il faut activer le debug mode
- appuyer plusieurs fois sur le "Numéro de build"
- aller dans les options de développeur
- activer le debuggage USB

## Debuggage
on peut ouvrir un onglet de debug de l'app en ouvrant le le lien `chrome://inspect#devices`   
depuis un google chrome sur mon pc.

## plugins
Pour accéder aux differentes fonctionnalités du téléphone, on passera par des plugins   
(on peut les retrouver sur la doc).   
En général, ils s'installent avec `cordova plugin add NOM_PLUGIN`   
leur utilisation varie d'un plugin a l'autre, il faudra lire la doc pour comprendre son fonctionnement.



## Publication

Pour publier une application sur le play store, il y a plusieures étapes à réaliser.

### Configs de l'app

Si ça n'a pas déja été fait, il faut renseigner les infos sur l'application dans le fichier config.xml
```xml
<?xml version='1.0' encoding='utf-8'?>
<!--widget id => id unique de l'app-->
<!--widget version => version de l'app, à changer à chaque version -->
<widget id="app.dunarr.musicplayer" version="1.0.0" xmlns="http://www.w3.org/ns/widgets" xmlns:cdv="http://cordova.apache.org/ns/1.0"> 
    <name>MusicPlayer</name> <!--Nom affiché pour l'application-->
    <description>A music App</description> <!--description utilisée sur certains menus dur tel-->
    <author email="dev@cordova.apache.org" href="https://cordova.apache.org"> <!--Infos sur le dev / la société qui publie-->
        Apache Cordova Team
    </author>
    <icon src="res/android/headphones-small.png" platform="android" width="64" height="64" density="mdpi" /> <!--logo de l'application-->
    <content src="index.html" />
    <allow-intent href="http://*/*" />
    <preference name="Scheme" value="http" />
    <allow-intent href="https://*/*" />
    <preference name="AndroidPersistentFileLocation" value="Compatibility" />
</widget>
```

/!\ le dossier "res" dans lequel se trouve l'icon est un dossier qui est à créer à la racine du projet.


### Compilation + signature

Pour publier une app, il nous faut une clé de signature (on peut en utiliser une préexistante)

Générer une clé: 
 - build > Générate signed bundle or apk > next > create new... (key store path)
 - il faut garder le key store path, l'alias et les 2 mots de passe
 
Pour compiuler l'application: 
 - `cordova build android --release -- --keystore=LIEN_DU_FICHIER_CLE --storePassword=MOT_DE_PASSE_1  --alias=ALIAS_DE_MA_CLE --password=MOT_DE_PASSE_1  --packageType=bundle --webpack.mode=production ` 

les infos LIEN_DU_FICHIER_CLE, MOT_DE_PASSE_1, ALIAS_DE_MA_CLE, MOT_DE_PASSE_1 correspondent aux infos récupérées a la génération de la clé


### Upload de l'app

Il reste enfin à uploader le fichier et à renseigner les infos demmandées par google. (https://play.google.com/console).
A savoir que via cette interface, on peut gérer les release, la com', le suivi et les sessions de test de notre app 
